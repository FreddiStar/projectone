/* 
 * File:   Texture2D.cpp
 * Author: Freddi
 * 
 * Created on 4. August 2013, 21:43
 */

#include "Texture2D.h"
#include "config.h"

Texture2D::Texture2D( const ImagePtr& p_img,
                      GLenum textureTarget, GLint mipmapLevel, GLint internalFormat,
                      GLenum pixelFormat, GLenum pixelType )
    : Texture( )
    , mp_imgData( p_img )
    , m_eTextureTarget( textureTarget )
    , m_iMipmapLevel( mipmapLevel )
    , m_iInternalFormat( internalFormat )
    , m_ePixelFormat( pixelFormat )
    , m_ePixelType( pixelType ) 
{
}

Texture2D::~Texture2D( ) { }

void
Texture2D::createTexture( )
{
    _ASSERT( mp_imgData );

    glGenTextures( 1, &m_uiTextureID );
    glBindTexture( GL_TEXTURE_2D, m_uiTextureID );

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glTexImage2D( GL_TEXTURE_2D,
                  0,
                  m_iInternalFormat,
                  mp_imgData->getWidth( ),
                  mp_imgData->getHeight( ),
                  0,
                  m_ePixelFormat,
                  m_ePixelType,
                  mp_imgData->getPixelData( ) );
}

