/* 
 * File:   MeshUtils.h
 * Author: Freddi
 *
 * Created on 17. Januar 2014, 17:01
 */

#ifndef MESHUTILS_H
#define	MESHUTILS_H

#include "Mesh.h"


namespace MeshUtils
{

    MeshPtr subdivide(MeshPtr p_in);

};

#endif	/* MESHUTILS_H */

