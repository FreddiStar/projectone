/* 
 * File:   Camera.cpp
 * Author: Freddi
 * 
 * Created on 4. August 2013, 20:56
 */

#include "Camera.h"
#include "Types.h"
#include "RendererComponent.h"

CameraPtr Camera::smp_currentCamera = CameraPtr( ); //(new Camera());

const CameraPtr&
Camera::getCurrent( )
{
    if ( !smp_currentCamera )
        smp_currentCamera.reset( new Camera( ) );
    return smp_currentCamera;
}

Camera::Camera( )
    : GameObject( )
    , m_fFieldOfView( 60.0f )
    , m_fNearClippingPlane( 0.1f )
    , m_fFarClippingPlane( 1000.0f )
    , m_matView( )
    , m_matProjection( )
    , m_iViewportWidth( 800 )
    , m_iViewportHeight( 600 )
    , m_eProjection( ePerspective )
    , m_bOrthographicOriginInCenter( false )
{
    updateProjectionMatrix( );
}

Camera::~Camera( ) { }

void Camera::init( )
{
    GameObject::init( );
}

void Camera::setActive( )
{
    smp_currentCamera = boost::dynamic_pointer_cast<Camera>(getThis( ));
}

void
Camera::setProjectionMods( enumCameraProjection eMode )
{
    m_eProjection = eMode;
    updateProjectionMatrix( );
}

void
Camera::updateProjectionMatrix( )
{
    if ( m_eProjection == ePerspective )
    {
        float fAspect = (m_iViewportWidth * 1.0f) / (m_iViewportHeight * 1.0f);
        m_matProjection = glm::perspective( m_fFieldOfView, fAspect, m_fNearClippingPlane, m_fFarClippingPlane );
    }
    else
    {
        float fEdges[4] = {0.0f, m_iViewportWidth * 1.0f, 0.0f, m_iViewportHeight * 1.0f};
        if ( m_bOrthographicOriginInCenter )
        {
            fEdges[0] = m_iViewportWidth * -0.5f;
            fEdges[1] = m_iViewportWidth * 0.5f;
            fEdges[2] = m_iViewportHeight * -0.5f;
            fEdges[3] = m_iViewportHeight * 0.5f;
        }

        m_matProjection = glm::ortho( fEdges[0], fEdges[1], fEdges[2],
                                      fEdges[3], m_fNearClippingPlane,
                                      m_fFarClippingPlane );
    }
}

void
Camera::setOrthographicOriginInCenter( bool b )
{
    m_bOrthographicOriginInCenter = b;
    updateProjectionMatrix( );
}

void
Camera::lookAt( const Vector3& at )
{
    TransformationPtr p_transformation = getTransformation( );
    if ( !p_transformation ) return;
    p_transformation->lookAt( at );

    m_matView = glm::lookAt( p_transformation->position( ),
                             at,
                             p_transformation->up( ) );

}