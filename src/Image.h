/* 
 * File:   Image.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 22:33
 */

#ifndef IMAGE_H
#define	IMAGE_H

#include <string>
#include "boost/shared_ptr.hpp"

using namespace std;

class Image;
typedef boost::shared_ptr<Image> ImagePtr;

class Image {
public:
    Image();
    virtual ~Image();
    
    static ImagePtr fromFile(const string& strFilename);
    void loadPixels(const unsigned char* p_pixels, int width, int height, int iBitPerPixel);
    
    int getWidth() const;
    int getHeight() const;
    int getBitPerPixel() const;
    int getBytePerPixel() const;
    
    const unsigned char* getPixelData() const;
private:
    int m_iWidth;
    int m_iHeight;
    
    int m_iBitPerPixel;
    
    unsigned char* mp_ucPixelData;
    
    void freePixelData();
    void allocatePixelData(int iWidth, int iHeight, int iBitPerPixel);
};

#endif	/* IMAGE_H */

