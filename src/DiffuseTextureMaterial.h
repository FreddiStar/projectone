/* 
 * File:   DiffuseTextureMaterial.h
 * Author: Freddi
 *
 * Created on 17. November 2013, 21:11
 */

#ifndef DIFFUSETEXTUREMATERIAL_H
#define	DIFFUSETEXTUREMATERIAL_H

#include "Material.h"


class DiffuseTextureMaterial;
typedef boost::shared_ptr<DiffuseTextureMaterial> DiffuseTextureMaterialPtr;

class DiffuseTextureMaterial : public Material {
public:
    DiffuseTextureMaterial(const Texture2DPtr& p_texture);
    virtual ~DiffuseTextureMaterial();
    
    bool loadShader();
    void setUniforms(const GameObjectPtr& p_obj);
    
    void getUniformsFromShader();
private:
    GLUniform       m_iTexture;
    Texture2DPtr    mp_tex2DColor;
};

#endif	/* DIFFUSETEXTUREMATERIAL_H */

