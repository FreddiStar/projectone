/* 
 * File:   TextureAtlas.cpp
 * Author: Freddi
 * 
 * Created on 28. Januar 2014, 14:39
 */

#include "TextureAtlas.h"

#include "tinyxml/tinyxml2.h"
using namespace tinyxml2;

TextureAtlas::TextureAtlas( )
    : m_strFilename( )
    , mp_imgAtlas( )
    , mp_texAtlas( )
    , m_mapIDToLocation( ) { }

TextureAtlas::~TextureAtlas( ) { }

TextureAtlasLocationPtr TextureAtlas::getByID( const string& c_strID )
{
    return TextureAtlasLocationPtr( );
}

bool TextureAtlas::load( const string& c_strFile )
{
    XMLDocument doc;
    if ( doc.LoadFile( c_strFile.c_str( ) ) == XML_NO_ERROR )
    {

        XMLElement* elem = doc.FirstChildElement( "atlas" );
        if ( elem )
        {
            const XMLAttribute* attr = elem->FirstAttribute( );
            while ( attr )
            {
                string strAttrName( attr->Name( ) );
                if ( strAttrName == "file" )
                {
                    m_strFilename = string( attr->Value( ) );
                    cout << "Loading Image for Atlas: " << m_strFilename << __LINE__ << endl;
                    mp_imgAtlas = Image::fromFile( m_strFilename );

                    if ( !mp_imgAtlas )
                    {
                        cout << "could not load image for atlas" << endl;
                        return false;
                    }
                }

                attr = attr->Next( );
            }

            XMLElement* tile = elem->FirstChildElement( "tile" );
            while ( tile )
            {

                attr = tile->FirstAttribute( );
                string strTileID;
                uint32_t uiLeft = 0, uiRight = 0, uiTop = 0, uiBottom = 0;
                while ( attr )
                {
                    string strAttrName( attr->Name( ) );

                    if ( strAttrName == "name" )
                    {
                        strTileID = string( attr->Value( )  );
                    }
                    else if ( strAttrName == "left" )
                    {
                        uiLeft = static_cast<uint32_t>(attr->IntValue( ));
                    }
                    else if ( strAttrName == "right" )
                    {
                        uiRight = static_cast<uint32_t>(attr->IntValue( ));
                    }
                    else if ( strAttrName == "top" )
                    {
                        uiTop = static_cast<uint32_t>(attr->IntValue( ));
                    }
                    else if ( strAttrName == "left" )
                    {
                        uiLeft = static_cast<uint32_t>(attr->IntValue( ));
                    }

                    attr = attr->Next( );
                }
                //                TextureAtlasLocationPtr p_atlasLocation( new TextureAtlasLocation( "", 0, 0, 0, 0, 0, 0 ) );
                TextureAtlasLocationPtr p_atlasLocation( new TextureAtlasLocation( strTileID,
                                                                                   mp_imgAtlas->getWidth(),
                                                                                   mp_imgAtlas->getHeight(),
                                                                                   uiLeft, uiRight,
                                                                                   uiTop, uiBottom ) );
                m_mapIDToLocation.insert( pair<string, TextureAtlasLocationPtr>(strTileID, p_atlasLocation) );

                tile = tile->NextSiblingElement( "tile" );
            }
            cout << "Node: " << elem->Value( ) << endl;
        }
        return true;
    }

    cerr << "Error loading Texture atlas: " << doc.ErrorID( ) << endl;

    return false;
}