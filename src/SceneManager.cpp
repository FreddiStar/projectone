/* 
 * File:   SceneManager.cpp
 * Author: Freddi
 * 
 * Created on 2. September 2013, 21:42
 */

#include "SceneManager.h"

SceneManager* SceneManager::smp_singleton = NULL;

SceneManager& SceneManager::get()
{
    if (!smp_singleton)
        smp_singleton = new SceneManager();
    
    return *smp_singleton;
}

SceneManager::SceneManager()
    : mp_scene( )
{
}

void SceneManager::destroy()
{
    if (smp_singleton)
        delete smp_singleton;
}

SceneManager::SceneManager(const SceneManager& orig) { }

void SceneManager::setScene(const ScenePtr& p_scene)
{
    if (mp_scene)
        mp_scene->unload();
    mp_scene = p_scene;
    
    mp_scene->setup();
}

ScenePtr SceneManager::getScene()
{
    return mp_scene;
}

SceneManager::~SceneManager() {
}

