/* 
 * File:   OpenGL.h
 * Author: frederiktrojahn
 *
 * Created on 11. August 2013, 19:09
 */

#ifndef OPENGL_H
#define	OPENGL_H

#ifdef __APPLE__

#include <GL/glew.h>

//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
//#include <OpenGL/glext.h>

#else
#include "GL/glew.h"
#include "GL/gl.h"

//#define GL_GLEXT_PROTOTYPES
//#include "SDL2/SDL_opengl.h"

#endif

#include "SDL2/SDL.h"

#include "stdlib.h"

#include <iostream>
using namespace std;

#include "config.h"


// GL-Types
typedef GLint GLUniform;

#ifdef GL_ERROR_CHECKING
inline void glCheckError(const string& msg)
{
    GLenum error = glGetError();
    while (error != GL_NO_ERROR)
    {
        cout << " OpenGL Error at " << msg << ": " << gluErrorString(error) << endl;
        error = glGetError();
    }
    _ASSERT(error == GL_NO_ERROR);
}
#else
    #define glCheckError(msg) ((void)0)
#endif

#ifdef GL_ERROR_CHECKING
    #define _ASSERT_NO_GL_ERROR() assert(glGetError() == GL_NO_ERROR);
#else
    #define _ASSERT_NO_GL_ERROR() ((void)0)
#endif

#endif	/* OPENGL_H */

