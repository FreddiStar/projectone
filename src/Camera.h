/* 
 * File:   Camera.h
 * Author: Freddi
 *
 * Created on 4. August 2013, 20:56
 */

#ifndef CAMERA_H
#define	CAMERA_H

#include "boost/shared_ptr.hpp"
#include "Types.h"
#include "GameObject.h"

class Camera;
typedef boost::shared_ptr<Camera> CameraPtr;

class Camera : public GameObject
{
public:

    enum enumCameraProjection
    {
        ePerspective,
        eOrthographic
    };

    Camera( );
    virtual ~Camera( );

    void setActive( );

    void init( );

    const Matrix4x4& getViewMatrix( ) const
    {
        return m_matView;
    }

    const Matrix4x4& getProjectionMatrix( ) const
    {
        return m_matProjection;
    }

    static const CameraPtr& getCurrent( );

    void lookAt( const Vector3& at );

    void setProjectionMods( enumCameraProjection eMode );
    
    void setOrthographicOriginInCenter(bool b);

private:

    static CameraPtr smp_currentCamera;

    float m_fFieldOfView;

    float m_fNearClippingPlane;
    float m_fFarClippingPlane;

    Matrix4x4 m_matView;
    Matrix4x4 m_matProjection;

    int m_iViewportWidth;
    int m_iViewportHeight;

    enumCameraProjection m_eProjection;
    
    bool m_bOrthographicOriginInCenter;

    void updateProjectionMatrix( );
};

#endif	/* CAMERA_H */

