/* 
 * File:   Types.h
 * Author: frederiktrojahn
 *
 * Created on 4. August 2013, 15:01
 */

#ifndef TYPES_H
#define	TYPES_H

#include <iostream>

using namespace std;

// BOOST
#include "boost/shared_ptr.hpp"
#include "boost/weak_ptr.hpp"

// GLM
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtx/quaternion.hpp"

typedef glm::vec2 Vector2;
typedef glm::uvec2 Vector2ui;

typedef glm::vec3 Vector3;
typedef glm::ivec3 Vector3i;

typedef glm::vec4 Vector4;
typedef glm::vec4 Color;

typedef glm::mat4x4 Matrix4x4;

typedef glm::quat Quaternion;

inline void print(const Matrix4x4& mat, const string& c_strName = "mat4x4")
{
  cout << c_strName << endl;
  for(int x = 0; x < 4; x++)
  {
    for(int y = 0; y < 4; y++)
    {
      cout << mat[x][y] << " ";
    }
    cout << endl;
  }
  cout << endl;
}

inline void print(const Vector3i& vec, const string& c_strName = "vec3i")
{
    cout << c_strName << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")" << endl;
}

inline void print(const Vector3& vec, const string& c_strName = "vec3")
{
    cout << c_strName << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")" << endl;
}

inline void print(const float& f, const string& c_strName = "float")
{
    cout << c_strName << " = " << f << endl;
}

inline void print(const Color& c, const string& c_strName = "color")
{
    cout << c_strName << "(" << c.r << ", " << c.g << ", " << c.b << ", " << c.a << ")" << endl;
}


#endif	/* TYPES_H */

