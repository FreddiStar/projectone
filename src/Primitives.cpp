/* 
 * File:   Primitives.cpp
 * Author: Freddi
 * 
 * Created on 29. August 2013, 22:23
 */

#include "Primitives.h"

Primitives::Primitives( ) { }

Primitives::~Primitives( ) { }

MeshPtr Primitives::QuadsPlane( int w, int h )
{
    MeshPtr p_mesh( new Mesh( GL_QUADS ) );
    
    int index = 0;

    for ( int y = 0; y < h; ++y )
    {
        for ( int x = 0; x < h; ++x )
        {
            p_mesh->pushVertex(Vector3(x*1.0f, y*1.0f, 0.0f));
            p_mesh->pushUV(Vector2(0.0f, 0.0f));
            
            p_mesh->pushVertex(Vector3((x+1)*1.0f, y*1.0f, 0.0f));
            p_mesh->pushUV(Vector2(1.0f, 0.0f));
            
            p_mesh->pushVertex(Vector3((x+1)*1.0f, (y+1)*1.0f, 0.0f));
            p_mesh->pushUV(Vector2(1.0f, 1.0f));
            
            p_mesh->pushVertex(Vector3(x*1.0f, (y+1)*1.0f, 0.0f));
            p_mesh->pushUV(Vector2(0.0f, 1.0f));
            
            p_mesh->pushColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
            p_mesh->pushColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
            p_mesh->pushColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
            p_mesh->pushColor(Color(1.0f, 1.0f, 1.0f, 1.0f));
            
            p_mesh->pushIndex(index++);
            p_mesh->pushIndex(index++);
            p_mesh->pushIndex(index++);
            p_mesh->pushIndex(index++);
        }
    }

    return p_mesh;
}

MeshPtr Primitives::Axis( )
{
    MeshPtr p_axis( new Mesh( GL_LINES ) );

    p_axis->pushVertex( Vector3( 0.0f, 0.0f, 0.0f ) );
    p_axis->pushColor( Color( 1.0f, 0.0f, 0.0f, 1.0f ) );
    p_axis->pushVertex( Vector3( 1.0f, 0.0f, 0.0f ) );
    p_axis->pushColor( Color( 1.0f, 0.0f, 0.0f, 1.0f ) );

    p_axis->pushVertex( Vector3( 0.0f, 0.0f, 0.0f ) );
    p_axis->pushColor( Color( 0.0f, 1.0f, 0.0f, 1.0f ) );
    p_axis->pushVertex( Vector3( 0.0f, 1.0f, 0.0f ) );
    p_axis->pushColor( Color( 0.0f, 1.0f, 0.0f, 1.0f ) );

    p_axis->pushVertex( Vector3( 0.0f, 0.0f, 0.0f ) );
    p_axis->pushColor( Color( 0.0f, 0.0f, 1.0f, 1.0f ) );
    p_axis->pushVertex( Vector3( 0.0f, 0.0f, 1.0f ) );
    p_axis->pushColor( Color( 0.0f, 0.0f, 1.0f, 1.0f ) );

    p_axis->pushIndex( 0 );
    p_axis->pushIndex( 1 );

    p_axis->pushIndex( 2 );
    p_axis->pushIndex( 3 );

    p_axis->pushIndex( 4 );
    p_axis->pushIndex( 5 );

    return p_axis;
}

MeshPtr Primitives::Box( )
{
    MeshPtr p_mesh( new Mesh( ) );

    Vector3 vertex0( -1.0f,  1.0f, -1.0f ); // 0
    Vector3 vertex1( -1.0f, -1.0f, -1.0f ); // 1
    Vector3 vertex2( 1.0f, -1.0f, -1.0f ); // 2
    Vector3 vertex3( 1.0f,  1.0f, -1.0f ); // 3

    Vector3 vertex4( -1.0f,  1.0f, 1.0f ); // 4
    Vector3 vertex5( -1.0f, -1.0f, 1.0f ); // 5
    Vector3 vertex6( 1.0f, -1.0f, 1.0f ); // 6
    Vector3 vertex7( 1.0f,  1.0f, 1.0f ); // 7

    Vector3 normal0( 0.0f, 0.0f, -1.0f );
    Vector3 normal1( -1.0f, 0.0f, 0.0f );
    Vector3 normal2( 0.0f, 0.0f, 1.0f );
    Vector3 normal3( 1.0f, 0.0f, 0.0f );
    Vector3 normal4( 0.0f, -1.0f, 0.0f );
    Vector3 normal5( 0.0f, 1.0f, 0.0f );

    Vector2 uv0( 0.0f, 1.0f );
    Vector2 uv1( 0.0f, 0.0f );
    Vector2 uv2( 1.0f, 0.0f );
    Vector2 uv3( 1.0f, 1.0f );

    // 0-1
    p_mesh->pushVertex( vertex0 );
    p_mesh->pushVertex( vertex1 );
    p_mesh->pushVertex( vertex3 );

    p_mesh->pushNormal( normal0 );
    p_mesh->pushNormal( normal0 );
    p_mesh->pushNormal( normal0 );

    // 0-2
    p_mesh->pushVertex( vertex1 );
    p_mesh->pushVertex( vertex2 );
    p_mesh->pushVertex( vertex3 );
    p_mesh->pushNormal( normal0 );
    p_mesh->pushNormal( normal0 );
    p_mesh->pushNormal( normal0 );

    // 1-1
    p_mesh->pushVertex( vertex0 );
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushVertex( vertex4 );
    p_mesh->pushNormal( normal1 );
    p_mesh->pushNormal( normal1 );
    p_mesh->pushNormal( normal1 );

    // 1-2
    p_mesh->pushVertex( vertex0 );
    p_mesh->pushVertex( vertex1 );
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushNormal( normal1 );
    p_mesh->pushNormal( normal1 );
    p_mesh->pushNormal( normal1 );

    // 2-1
    p_mesh->pushVertex( vertex4 );
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushVertex( vertex7 );
    p_mesh->pushNormal( normal2 );
    p_mesh->pushNormal( normal2 );
    p_mesh->pushNormal( normal2 );

    // 2-2
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushVertex( vertex6 );
    p_mesh->pushVertex( vertex7 );
    p_mesh->pushNormal( normal2 );
    p_mesh->pushNormal( normal2 );
    p_mesh->pushNormal( normal2 );

    // 3-1
    p_mesh->pushVertex( vertex7 );
    p_mesh->pushVertex( vertex6 );
    p_mesh->pushVertex( vertex3 );
    p_mesh->pushNormal( normal3 );
    p_mesh->pushNormal( normal3 );
    p_mesh->pushNormal( normal3 );

    // 3-2
    p_mesh->pushVertex( vertex6 );
    p_mesh->pushVertex( vertex2 );
    p_mesh->pushVertex( vertex3 );
    p_mesh->pushNormal( normal3 );
    p_mesh->pushNormal( normal3 );
    p_mesh->pushNormal( normal3 );

    // 4-1
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushVertex( vertex1 );
    p_mesh->pushVertex( vertex2 );
    p_mesh->pushNormal( normal4 );
    p_mesh->pushNormal( normal4 );
    p_mesh->pushNormal( normal4 );

    // 4-2
    p_mesh->pushVertex( vertex5 );
    p_mesh->pushVertex( vertex2 );
    p_mesh->pushVertex( vertex6 );
    p_mesh->pushNormal( normal4 );
    p_mesh->pushNormal( normal4 );
    p_mesh->pushNormal( normal4 );

    // 5-1
    p_mesh->pushVertex( vertex0 );
    p_mesh->pushVertex( vertex4 );
    p_mesh->pushVertex( vertex7 );
    p_mesh->pushNormal( normal5 );
    p_mesh->pushNormal( normal5 );
    p_mesh->pushNormal( normal5 );

    // 5-2
    p_mesh->pushVertex( vertex0 );
    p_mesh->pushVertex( vertex7 );
    p_mesh->pushVertex( vertex3 );
    p_mesh->pushNormal( normal5 );
    p_mesh->pushNormal( normal5 );
    p_mesh->pushNormal( normal5 );

    return p_mesh;
}

MeshPtr Primitives::Sphere( )
{
    return MeshPtr( );
}

MeshPtr Primitives::Plane( int iSubdivisions )
{
    MeshPtr p_plane( new Mesh( ) );

    // Vertices
    p_plane->pushVertex( Vector3( 1.0f, 1.0f, 0.0f ) );
    p_plane->pushVertex( Vector3( -1.0f, 1.0f, 0.0f ) );
    p_plane->pushVertex( Vector3( -1.0f, -1.0f, 0.0f ) );

    p_plane->pushVertex( Vector3( 1.0f, 1.0f, 0.0f ) );
    p_plane->pushVertex( Vector3( -1.0f, -1.0f, 0.0f ) );
    p_plane->pushVertex( Vector3( 1.0f, -1.0f, 0.0f ) );

    // Normals
    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );
    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );
    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );

    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );
    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );
    p_plane->pushNormal( Vector3( 0.0f, 0.0f, 1.0f ) );

    // UV
    p_plane->pushUV( Vector2( 1.0f, 1.0f ) );
    p_plane->pushUV( Vector2( 0.0f, 1.0f ) );
    p_plane->pushUV( Vector2( 0.0f, 0.0f ) );

    p_plane->pushUV( Vector2( 1.0f, 1.0f ) );
    p_plane->pushUV( Vector2( 0.0f, 0.0f ) );
    p_plane->pushUV( Vector2( 1.0f, 0.0f ) );

    // Farbe
    p_plane->pushColor( Vector4( 1.0f, 0.0f, 0.0f, 1.0f ) );
    p_plane->pushColor( Vector4( 0.0f, 1.0f, 0.0f, 1.0f ) );
    p_plane->pushColor( Vector4( 0.0f, 0.0f, 1.0f, 1.0f ) );

    p_plane->pushColor( Vector4( 0.0f, 1.0f, 1.0f, 1.0f ) );
    p_plane->pushColor( Vector4( 1.0f, 0.0f, 1.0f, 1.0f ) );
    p_plane->pushColor( Vector4( 1.0f, 1.0f, 0.0f, 1.0f ) );

    // Indices
    p_plane->pushIndex( 0 );
    p_plane->pushIndex( 1 );
    p_plane->pushIndex( 2 );

    p_plane->pushIndex( 3 );
    p_plane->pushIndex( 4 );
    p_plane->pushIndex( 5 );

    return p_plane;
}

MeshPtr Primitives::Cylinder( int iResolution )
{
    return MeshPtr( );
}
