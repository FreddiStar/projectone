/* 
 * File:   Light.h
 * Author: Freddi
 *
 * Created on 8. August 2013, 21:15
 */

#ifndef LIGHT_H
#define	LIGHT_H

#include "boost/shared_ptr.hpp"
#include "Types.h"

class Light;
typedef boost::shared_ptr<Light> LightPtr;

class Light
{
public:

    enum enumType
    {
        eTypeSpot,
        eTypeDirectional,
        eTypePoint
        //eTypeArea
    };
    Light( );
    virtual ~Light( );
private:
    enumType m_eType;
    float m_fRange;
    Vector3 m_vec3Color;
    float m_fSpotAngle;
    float m_fIntensity;
};

#endif	/* LIGHT_H */

