/* 
 * File:   DiffuseVertexColorMaterial.cpp
 * Author: Freddi
 * 
 * Created on 17. Oktober 2013, 20:26
 */

#include "DiffuseVertexColorMaterial.h"

DiffuseVertexColorMaterial::DiffuseVertexColorMaterial()
    : Material()
{
}

DiffuseVertexColorMaterial::~DiffuseVertexColorMaterial() {
}

bool DiffuseVertexColorMaterial::loadShader()
{
    mp_shader = Shader::fromFilePair("data/materials/diffusevertexcolor");

    if (!mp_shader)
    {
        cout << "DiffuseVertexColorMaterial::loadShader could not load shader" << endl;
        return false;
    }
    
    return true;
}