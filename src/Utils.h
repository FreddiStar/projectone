/* 
 * File:   Utils.h
 * Author: frederiktrojahn
 *
 * Created on 6. Oktober 2013, 15:11
 */

#ifndef UTILS_H
#define	UTILS_H

#include <string>
#include <sstream>
#include <stdlib.h>

using namespace std;

template<typename T>
void safeDelete( T*& ptr )
{
    if ( ptr )
    {
        delete ptr;
        ptr = NULL;
    }
}

template<typename T>
void safeDeleteArray( T*& ptr )
{
    if ( ptr )
    {
        delete[] ptr;
        ptr = NULL;
    }
}

template <class T>
string toString( const T& value )
{
    ostringstream out;
    out << value;
    return out.str();
}

namespace Math
{

    template <class T>
    T min( T t0, T t1 )
    {
        return (t0 > t1) ? t1 : t0;
    }

    template <class T>
    T max( T t0, T t1 )
    {
        return (t0 > t1) ? t0 : t1;
    }

}

#endif	/* UTILS_H */

