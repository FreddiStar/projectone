/* 
 * File:   DiffuseTextureMaterial.cpp
 * Author: Freddi
 * 
 * Created on 17. November 2013, 21:11
 */

#include "DiffuseTextureMaterial.h"

DiffuseTextureMaterial::DiffuseTextureMaterial(const Texture2DPtr& p_texture)
    : Material()
    , m_iTexture( 0 )
    , mp_tex2DColor( p_texture )
{
}

DiffuseTextureMaterial::~DiffuseTextureMaterial() {
}

bool DiffuseTextureMaterial::loadShader()
{
    mp_shader = Shader::fromFilePair("data/materials/diffusetexture");

    if (!mp_shader)
    {
        cout << "DiffuseTextureMaterial::loadShader could not load shader" << endl;
        return false;
    }
    
    return true;
}

void DiffuseTextureMaterial::getUniformsFromShader()
{
    Material::getUniformsFromShader();
    
    m_iTexture = mp_shader->getUniform(ShaderVariables::UniformColorTexture2D);
    if (!m_iTexture)
    {
       cout << "DiffuseTextureMaterial::getUniformsFromShader could not find uniform "
               << ShaderVariables::UniformColorTexture2D << endl;
    }
}

void DiffuseTextureMaterial::setUniforms(const GameObjectPtr& p_obj)
{
    Material::setUniforms(p_obj);
    if (mp_shader) return;
    
    mp_shader->setUniform(m_iTexture, mp_tex2DColor, 1);
}