/* 
 * File:   FreeCamera.h
 * Author: Freddi
 *
 * Created on 20. November 2013, 22:12
 */

#ifndef FREECAMERA_H
#define	FREECAMERA_H

#include "Camera.h"
#include "InputListener.h"

class FreeCamera;
typedef boost::shared_ptr<FreeCamera> FreeCameraPtr;

class FreeCamera : public Camera, public KeyboardInputListener, public MouseInputListener {
public:
    FreeCamera();
    virtual ~FreeCamera();
    
    virtual bool onKeyPress( KeyboardInputArgs& args );
    virtual bool onKeyRelease( KeyboardInputArgs& args );
    
    virtual void update( );

private:
    
    enum eMovementDirections {
        eMoveBack = 1,
        eDontMove = 0,
        eMove = -1
    };
    
    float m_fMovementSpeed;
    float m_fRotationSpeed;
    
    Vector3 m_vec3Movement;
    float   m_fRotation;
};

#endif	/* FREECAMERA_H */

