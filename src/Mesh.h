/* 
 * File:   Mesh.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 13:31
 */

#ifndef MESH_H
#define	MESH_H

#include <vector>

#include "boost/shared_ptr.hpp"
#include "Types.h"
#include "OpenGL.h"
#include "Shader.h"
#include "MeshAttribute.h"

using namespace std;

class Mesh;
typedef boost::shared_ptr<Mesh> MeshPtr;

class Mesh
{
public:
    Mesh( GLenum eDrawMode = GL_TRIANGLES );
    virtual ~Mesh( );

    bool isDataChanged( ) const;

    void pushVertex( const Vector3& vertex );
    void pushNormal( const Vector3& normal );
    void pushIndex( GLuint index );
    void pushUV( const Vector2& uv );
    void pushColor( const Color& c );

    void draw( );

    bool isVertexDataEnabled( ) const;
    bool isNormalDataEnabled( ) const;
    bool isUVDataEnabled( ) const;
    bool isColorDataEnabled( ) const;

    void enableAttributes( );
    void disableAttributes( );

    void setEnableVertices( bool b );
    void setEnableNormals( bool b );
    void setEnableUV( bool b );
    void setEnableColors( bool b );
    
    const MeshAttributeGLuintGLuintPtr& getIndexData();
    
    const MeshAttributeVector3FloatPtr& getVertexData();
    const MeshAttributeVector3FloatPtr& getNormalData();
    const MeshAttributeVector2FloatPtr& getUVData();
    const MeshAttributeColorFloatPtr&   getColorData();

private:

    GLuint m_uiBufferID;
    GLuint m_uiIndexBufferID;

    MeshAttributeGLuintGLuintPtr mp_indicesAttribute;

    MeshAttributeVector3FloatPtr mp_verticesAttribute;
    MeshAttributeVector3FloatPtr mp_normalsAttribute;
    MeshAttributeVector2FloatPtr mp_uvAttribute;
    MeshAttributeColorFloatPtr mp_colorAttribute;

    vector<MeshAttributeAbstractPtr> m_vecAttributes;

    GLenum m_eDrawMode;

    void updateData( );

    void updateVertexData( );
    void updateNormalData( );
    void updateUVData( );
    void updateColorData( );
    void updateIndexData( );

    void createVBO( );
    void deleteVBO( );
    void updateVBO( );

    void bindVBO( );
    void unbindVBO( );
};

#endif	/* MESH_H */

