/* 
 * File:   Texture.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 13:32
 */

#ifndef TEXTURE_H
#define	TEXTURE_H

#include "boost/shared_ptr.hpp"
#include "OpenGL.h"

class Texture;
typedef boost::shared_ptr<Texture> TexturePtr;

class Texture
{
public:
    Texture( );
    virtual ~Texture( );

    GLuint getTexture( ) const;

    virtual void createTexture( )
    {
    }

protected:
    GLuint m_uiTextureID;
};

#endif	/* TEXTURE_H */

