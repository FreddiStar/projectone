/* 
 * File:   Light.h
 * Author: Freddi
 *
 * Created on 4. August 2013, 21:50
 */

#ifndef LIGHTCOMPONENT_H
#define	LIGHTCOMPONENT_H

#include "Component.h"
#include "Light.h"

class LightComponent;
typedef boost::shared_ptr<LightComponent> LightComponentPtr;

class LightComponent : public Component {
public:
    LightComponent();
    virtual ~LightComponent();
    
    const LightPtr& getLight() const { return mp_light; }
    void setLight(const LightPtr& p_light) { mp_light = p_light; }
private:
    LightPtr    mp_light;
};

#endif	/* LIGHTCOMPONENT_H */

