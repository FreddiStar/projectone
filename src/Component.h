/* 
 * File:   Component.h
 * Author: frederiktrojahn
 *
 * Created on 2. August 2013, 17:44
 */

#ifndef COMPONENT_H
#define	COMPONENT_H


#include "Types.h"
#include "boost/enable_shared_from_this.hpp"

class GameObject;
typedef boost::shared_ptr<GameObject> GameObjectPtr;
typedef boost::weak_ptr<GameObject> GameObjectWPtr;

class Component;
typedef boost::shared_ptr<Component> ComponentPtr;

class Component : public boost::enable_shared_from_this<Component> {
public:
    Component();
    virtual ~Component();
    
    ComponentPtr getThis() { return shared_from_this(); }
    
    virtual void update() { }
    virtual void awake() { }
    
    GameObjectPtr getGameObject();
    void setGameObject(const GameObjectPtr& p_obj);
    
    void enable();
    void disable();
    bool isEnabled();
private:
    GameObjectWPtr mwp_gameObject;
    bool m_bEnabled;
};

#endif	/* COMPONENT_H */

