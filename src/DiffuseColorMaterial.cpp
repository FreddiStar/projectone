/* 
 * File:   DiffuseColorMaterial.cpp
 * Author: Freddi
 * 
 * Created on 2. September 2013, 19:47
 */

#include "DiffuseColorMaterial.h"

DiffuseColorMaterial::DiffuseColorMaterial(const Vector4& color) 
    : Material()
    , m_iUniformColor( 0 )
    , m_vec4Color( color )
{
}

DiffuseColorMaterial::~DiffuseColorMaterial() {
}

bool DiffuseColorMaterial::loadShader()
{
    mp_shader = Shader::fromFilePair("data/materials/diffusecolor");

    if (!mp_shader)
    {
        cout << "DiffuseColorMaterial::loadShader could not load shader" << endl;
        return false;
    }
    
    return true;
}

void DiffuseColorMaterial::getUniformsFromShader()
{
    Material::getUniformsFromShader();
    
    m_iUniformColor = mp_shader->getUniform(ShaderVariables::UniformDiffuseColorVec4);
    if (!m_iUniformColor)
    {
       cout << "DiffuseColorMaterial::getUniformsFromShader could not find uniform "
               << ShaderVariables::UniformDiffuseColorVec4 << endl;
    }
}

void DiffuseColorMaterial::setUniforms(const GameObjectPtr& p_obj)
{
    Material::setUniforms(p_obj);
    if (!mp_shader) return;
    
    if (m_iUniformColor)
    {
        mp_shader->setUniform(m_iUniformColor, m_vec4Color);
    }
        
}

void DiffuseColorMaterial::setColor(const Vector4& color)
{
    m_vec4Color = color;
}

const Vector4& DiffuseColorMaterial::getColor() const
{
    return m_vec4Color;
}