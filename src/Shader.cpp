/* 
 * File:   Shader.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 13:32
 */

#include "OpenGL.h"

#include "Shader.h"
#include "FileUtils.h"
#include "config.h"

const string ShaderVariables::UniformMVPMatrix          = "mat4MVP";
const string ShaderVariables::UniformModelMatrix        = "mat4Model";
const string ShaderVariables::UniformViewMatrix         = "mat4View";
const string ShaderVariables::UniformProjectionMatrix   = "mat4Projection";

// Uniforms
const string ShaderVariables::UniformDiffuseColorVec4   = "vec4DiffuseColor";

// Textures
const string ShaderVariables::UniformColorTexture2D     = "tex2dColor";
const string ShaderVariables::UniformNormalMap2D        = "tex2dNormal";
const string ShaderVariables::UniformSpecularMap2D      = "tex2dSpecular";
const string ShaderVariables::UniformBumpMap2D          = "tex2dBump";
const string ShaderVariables::UniformDisplacementMap2D  = "tex2dDisplacment";
const string ShaderVariables::UniformEnvironmentMap2D   = "tex2dEnvironment";

// Attributes
const string ShaderVariables::AttributeVertexVec3       = "vec3Vertex";
const string ShaderVariables::AttributeColorVec4        = "vec4Color";
const string ShaderVariables::AttributeUVVec2           = "vec2UV";
const string ShaderVariables::AttributeNormalVec3       = "vec3Normal";

const GLint  ShaderVariables::AttributeLocationVertex   = 0;
const GLint  ShaderVariables::AttributeLocationColor    = 1;
const GLint  ShaderVariables::AttributeLocationUV       = 2;
const GLint  ShaderVariables::AttributeLocationNormal   = 3;

string Shader::FILE_EXT_FRAG = "frag";
string Shader::FILE_EXT_VERT = "vert";

GLuint createShader( const char* cp_data, GLenum eShaderType )
{
    _ASSERT( cp_data != NULL );

    GLuint iShader = glCreateShader( eShaderType );
    GLint iCompileStatus = GL_FALSE;

    glShaderSource( iShader, 1, &cp_data, NULL );
    glCompileShader( iShader );
    glCheckError( "glCompileShader " );
    glGetShaderiv( iShader, GL_COMPILE_STATUS, &iCompileStatus );

    if ( !iCompileStatus )
    {
        string strType = "Fragment";
        if ( eShaderType == GL_VERTEX_SHADER )
            strType = "Vertex";
        cout << "Shader::createShader: Could not compile " << strType << " shader!" << endl;

        GLint iLength = 0;
        GLsizei iSize = 0;
        glGetShaderiv( iShader, GL_INFO_LOG_LENGTH, &iLength );

        if ( iLength > 1 )
        {
            //#ifdef __WIN32
            GLchar* buffer = (GLchar*)malloc( sizeof (GLchar) * iLength );
            glGetShaderInfoLog( iShader, iLength, &iSize, buffer );
            cout << "COMPILE ERROR: " << buffer << endl;
            free( buffer );
            //#endif
        }

        glDeleteShader( iShader );

        return 0;
    }


    return iShader;
}

Shader::Shader( )
    : m_iProgramID( 0 ) { }

Shader::~Shader( )
{
    if ( m_iProgramID != 0 )
        glDeleteProgram( m_iProgramID );
}

ShaderPtr
Shader::fromFilePair( const string& c_strVertex, const string& c_strFrag )
{
    string strFragFile = c_strVertex + "." + FILE_EXT_FRAG;
    string strVertFile;
    if ( c_strFrag == "" )
        strVertFile = c_strVertex + "." + FILE_EXT_VERT;
    else
        strVertFile = c_strFrag + "." + FILE_EXT_VERT;

    if ( !File::exists( strFragFile ) || !File::exists( strVertFile ) )
        return ShaderPtr( );

    string strVertexData = File::load( strVertFile );
    string strFragmentData = File::load( strFragFile );

    return fromMemory( strVertexData.c_str( ), strFragmentData.c_str( ) );
}

ShaderPtr
Shader::fromMemory( const char* cp_dataVertex, const char* cp_dataFrag )
{
    _ASSERT( cp_dataVertex );
    _ASSERT( cp_dataFrag );

    ShaderPtr p_shader( new Shader( ) );
    p_shader->m_iProgramID = glCreateProgram( );

    GLuint uiVertShader = createShader( cp_dataVertex, GL_VERTEX_SHADER );
    GLuint uiFragShader = createShader( cp_dataFrag, GL_FRAGMENT_SHADER );

    if ( uiVertShader != 0 && uiFragShader != 0 )
    {
        glAttachShader( p_shader->m_iProgramID, uiVertShader );
        glCheckError( "glAttachShader uiVertShader" );
        glAttachShader( p_shader->m_iProgramID, uiFragShader );
        glCheckError( "glAttachShader uiFragShader" );

        glBindAttribLocation( p_shader->m_iProgramID, ShaderVariables::AttributeLocationVertex, ShaderVariables::AttributeVertexVec3.c_str( ) );
        glCheckError( "glBindAttribLocation Vertex" );
        glBindAttribLocation( p_shader->m_iProgramID, ShaderVariables::AttributeLocationNormal, ShaderVariables::AttributeNormalVec3.c_str( ) );
        glCheckError( "glBindAttribLocation Normal" );
        glBindAttribLocation( p_shader->m_iProgramID, ShaderVariables::AttributeLocationUV, ShaderVariables::AttributeUVVec2.c_str( ) );
        glCheckError( "glBindAttribLocation UV" );
        glBindAttribLocation( p_shader->m_iProgramID, ShaderVariables::AttributeLocationColor, ShaderVariables::AttributeColorVec4.c_str( ) );
        glCheckError( "glBindAttribLocation Color" );

        GLint iLinkStatus = GL_FALSE;
        glLinkProgram( p_shader->m_iProgramID );
        glCheckError( "glLinkProgram" );

        glDeleteShader( uiVertShader );
        glDeleteShader( uiFragShader );

        glGetProgramiv( p_shader->m_iProgramID, GL_LINK_STATUS, &iLinkStatus );
        if ( !iLinkStatus )
        {
            cout << "Could not link Shader. " << endl;

            GLint iLength = 0;
            GLsizei iSize = 0;
            glGetProgramiv( p_shader->m_iProgramID, GL_INFO_LOG_LENGTH, &iLength );
            if ( iLength > 1 )
            {
                //#ifdef __WIN32
                GLchar* buffer = new GLchar[iLength];
                glGetInfoLogARB( p_shader->m_iProgramID, iLength, &iSize, buffer );
                string sError( buffer, iLength );
                cout << "LINK ERROR: " << sError << endl;
                delete[] buffer;
                //#endif
            }

            return ShaderPtr( );
        }

        glValidateProgram( p_shader->m_iProgramID );
        GLint status;
        glGetProgramiv( p_shader->m_iProgramID, GL_VALIDATE_STATUS, &status );
        if ( status == GL_FALSE )
        {
            GLchar buffer[1024];
            GLint iSize = 0;
            glGetProgramInfoLog( p_shader->m_iProgramID, 1024, &iSize, buffer );
            cerr << "Error validating shader " << p_shader->m_iProgramID << endl;
            cerr << string( buffer, iSize ) << endl;
        }

        return p_shader;
    }

    if ( uiVertShader )
    {
        glDeleteShader( uiVertShader );
    }

    if ( uiFragShader )
    {
        glDeleteShader( uiFragShader );
    }

    glCheckError( "Shader::fromMemory" );
    return ShaderPtr( );
}

GLint
Shader::getUniform( const string& c_strUniform )
{
    _ASSERT( m_iProgramID != 0 );

    GLint loc = glGetUniformLocation( m_iProgramID, c_strUniform.c_str( ) );
    cout << "Uniform: " << c_strUniform << " at " << loc << endl;
    glCheckError( "Shader::getUniform" );
    return loc;
}

GLint
Shader::getAttribute( const string& c_strAttrib )
{
    _ASSERT( m_iProgramID != 0 );

    GLint loc = glGetAttribLocation( m_iProgramID, c_strAttrib.c_str( ) );
    cout << "Attribute: " << c_strAttrib << " at " << loc << endl;
    glCheckError( "Shader::getAttribute" );
    return loc;
}

void
Shader::setUniform( GLint iLocation, const Matrix4x4& mat )
{
    _ASSERT( m_iProgramID != 0 );

    glUniformMatrix4fv( iLocation, 1, GL_FALSE, glm::value_ptr( mat ) );

    glCheckError( "Shader::setUniform mat" );
}

void
Shader::setUniform( GLint iLocation, const Vector4& vec4 )
{
    _ASSERT( m_iProgramID != 0 );

    glUniform4fv( iLocation, 1, glm::value_ptr( vec4 ) );
    glCheckError( "Shader::setUniform vec4" );
}

void
Shader::setUniform( GLint iLocation, const Vector3& vec3 )
{
    _ASSERT( m_iProgramID != 0 );

    glUniform4fv( iLocation, 1, glm::value_ptr( vec3 ) );
}

void
Shader::setUniform( GLint iLocation, const Vector2& vec2 )
{
    _ASSERT( m_iProgramID != 0 );

    glUniform4fv( iLocation, 1, glm::value_ptr( vec2 ) );
}

void
Shader::setUniform( GLint iLocation, const Texture2DPtr& tex2d, GLint iTextureUnit )
{
    _ASSERT( m_iProgramID != 0 );

    glUniform1i( iLocation, iTextureUnit );

    glActiveTexture( GL_TEXTURE0 + iTextureUnit );
    glBindTexture( GL_TEXTURE_2D, tex2d->getTexture( ) );
}

void
Shader::begin( )
{
    _ASSERT( m_iProgramID != 0 );

    glUseProgram( m_iProgramID );

    glCheckError( "Shader::begin" );
}

void
Shader::end( )
{
    glUseProgram( 0 );
    glCheckError( "Shader::end" );
}
