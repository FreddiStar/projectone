/* 
 * File:   LuaState.h
 * Author: Freddi
 *
 * Created on 28. November 2013, 20:37
 */

#ifndef LUASTATE_H
#define	LUASTATE_H

#include "lua.hpp"
#include "boost/shared_ptr.hpp"

class LuaState;
typedef boost::shared_ptr<LuaState> LuaStatePtr;

class LuaState {
public:
    
    static LuaStatePtr get();
    static void destory();
    
    virtual ~LuaState();
private:
    LuaState();
    
    lua_State* mp_state;
    
    static LuaStatePtr mp_singleton;
};

#endif	/* LUASTATE_H */

