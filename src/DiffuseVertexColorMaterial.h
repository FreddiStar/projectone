/* 
 * File:   DiffuseVertexColorMaterial.h
 * Author: Freddi
 *
 * Created on 17. Oktober 2013, 20:26
 */

#ifndef DIFFUSEVERTEXCOLORMATERIAL_H
#define	DIFFUSEVERTEXCOLORMATERIAL_H

#include "Material.h"

class DiffuseVertexColorMaterial;
typedef boost::shared_ptr<DiffuseVertexColorMaterial> DiffuseVertexColorMaterialPtr;

class DiffuseVertexColorMaterial : public Material {
public:
    DiffuseVertexColorMaterial();
    virtual ~DiffuseVertexColorMaterial();
    
    bool loadShader();
private:

};

#endif	/* DIFFUSEVERTEXCOLORMATERIAL_H */

