/* 
 * File:   FileUtils.cpp
 * Author: Freddi
 * 
 * Created on 9. August 2013, 21:26
 */

#include "FileUtils.h"
#include "config.h"
#include <fstream>
#include <sstream>
#include <sys/stat.h>

using namespace std;

string 
File::load(const string& c_strFile)
{
    
    ifstream fs( c_strFile.c_str() );
    if (!fs.good())
    {
        _VERBOSE_LOG("Error loading " << c_strFile);
        return string("");
    }
    
    return std::string((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());
}

bool 
File::exists(const string& c_strFile)
{
    struct stat buf;
    if (stat(c_strFile.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

//bool 
//File::save(const string& c_strFile, const string& c_strContent)
//{
//    ofstream os(c_strFile.c_str(), ofstream::out|ofstream::app);
//    if (os.good())
//    {
//        os << c_strContent;
//    }
//    os.close();
//}

