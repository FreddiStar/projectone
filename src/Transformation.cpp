/* 
 * File:   Transformation.cpp
 * Author: frederiktrojahn
 * 
 * Created on 2. August 2013, 18:45
 */

#include "Transformation.h"
#include "Mathf.h"

Transformation::Transformation( )
    : Component( )
    , mwp_parent( )
    , m_listChildren( )
    , m_bTransformChanged( true )
    , m_matTransformation( )
    , m_vec3Position( 0.0f, 0.0f, 0.0f )
    , m_qatRotation( )
    , m_vec3Scale( 1.0f, 1.0f, 1.0f ) { }

Transformation::~Transformation( ) { }

TransformationPtr
Transformation::parent( )
{
    return mwp_parent.lock( );
}

TransformationPtr
Transformation::root( )
{
    TransformationPtr p_parent = parent( );
    if ( p_parent )
    {
        return p_parent->root( );
    }

    return boost::dynamic_pointer_cast<Transformation>(getThis( ));
}

void
Transformation::rotate( float fAngle, const Vector3& axis )
{
    //    print( axis, "axis" );
    m_qatRotation = glm::rotate( m_qatRotation, fAngle, glm::normalize( axis ) );
    m_bTransformChanged = true;
}

const Matrix4x4&
Transformation::getMatrix( )
{
    if ( m_bTransformChanged )
        updateTransformationMatrix( );

    return m_matTransformation;
}

void
Transformation::addChild( const TransformationPtr& p_transform )
{
    if ( !p_transform ) return;

    if ( p_transform->parent( ) )
    {
        p_transform->parent( )->removeChild( p_transform );
    }

    m_listChildren.push_back( p_transform );
    p_transform->mwp_parent = boost::dynamic_pointer_cast<Transformation>(getThis( ));
}

const Transformation::TransformationPtrList&
Transformation::getChildren( ) const
{
    return m_listChildren;
}

Vector3
Transformation::localUp( )
{
    return Vector3( 0.0f, 1.0f, 0.0f );
}

Vector3
Transformation::localForward( )
{
    return Vector3( 0.0f, 0.0f, -1.0f );
}

Vector3
Transformation::localRight( )
{
    return Vector3( 1.0f, 0.0f, 0.0f );
}

Vector3
Transformation::up( )
{
    return glm::normalize( transformDirection( localUp( ) ) );
}

Vector3
Transformation::forward( )
{
    return glm::normalize( transformDirection( localForward( ) ) );
}

Vector3
Transformation::right( )
{
    return glm::normalize( transformDirection( localRight( ) ) );
}

Vector3
Transformation::transformPosition( const Vector3& pos )
{
    return Vector3( getMatrix( ) * Vector4( pos, 1.0f ) );
}

Vector3
Transformation::transformDirection( const Vector3& dir )
{
    return Vector3( m_qatRotation * Vector4( dir, 0.0f ) );
}

void
Transformation::removeChild( const TransformationPtr& p_transform )
{
    if ( !p_transform ) return;

    // @todo
    m_listChildren.remove( p_transform );
    p_transform->mwp_parent = TransformationPtr( );
}

//void
//Transformation::update( )
//{
//    Component::update( );
//
//    if ( m_bTransformChanged )
//        updateTransformationMatrix( );
//}

Vector3&
Transformation::position( )
{
    m_bTransformChanged = true;
    return m_vec3Position;
}

Quaternion&
Transformation::rotation( )
{
    m_bTransformChanged = true;
    return m_qatRotation;
}

Vector3&
Transformation::scale( )
{
    m_bTransformChanged = true;
    return m_vec3Scale;
}

void
Transformation::updateTransformationMatrix( )
{
    Matrix4x4 matTranslation = glm::translate( m_vec3Position );
    Matrix4x4 matScale = glm::scale( m_vec3Scale );
    Matrix4x4 matRotation = glm::toMat4( m_qatRotation );

    m_matTransformation = matTranslation * matRotation * matScale;
    m_bTransformChanged = false;
}

Matrix4x4 Transformation::localToGlobalMatrix( )
{
    //    TransformationPtr p_parent = mwp_parent.lock( );
    //    if ( p_parent )
    //    {
    //        return getMatrix() * p_parent->localToGlobalMatrix( );
    //    }
    //    else
    return getMatrix( );
}

Matrix4x4
Transformation::getModelMatrix( )
{
    //    TransformationPtr p_parent = mwp_parent.lock( );
    //    if ( !p_parent )
    return getMatrix( );
    //    else
    //    {
    //        return p_parent->getModelMatrix( ) * getMatrix();
    //    }
}

void Transformation::lookAt( const Vector3& at )
{
    //    assert( at != position );

    Vector3 direction = glm::normalize( at - position( ) );
    float dot = glm::dot( localForward( ), direction );
    if ( fabs( dot + 1.0f ) < 0.000001f )
    {
        m_qatRotation = glm::angleAxis( Mathf::rad2deg( M_PI ), localUp( ) );
        return;
    }
    else if ( fabs( dot - 1.0f ) < 0.000001f )
    {
        m_qatRotation = Quaternion( );
        return;
    }

    float angle = -Mathf::rad2deg( acosf( dot ) );

    Vector3 cross = glm::normalize( glm::cross( localForward( ), direction ) )*-1.0f;
    //    print( cross, "cross" );
    m_qatRotation = glm::normalize( glm::angleAxis( angle, cross ) );

    m_bTransformChanged = true;
}