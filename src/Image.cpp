/* 
 * File:   Image.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 22:33
 */

#include "Image.h"

#include "FreeImage.h"
#include <stdio.h>
#include <string.h>

Image::Image()
  : m_iWidth( 0 )
  , m_iHeight( 0 )
  , m_iBitPerPixel( 0 )
  , mp_ucPixelData( NULL )
{
}

Image::~Image() {
}

int
Image::getWidth() const
{
    return m_iWidth;
}

int 
Image::getHeight() const
{
    return m_iHeight;
}

int  
Image::getBitPerPixel() const
{
    return m_iBitPerPixel;
}

int  
Image::getBytePerPixel() const
{
    return m_iBitPerPixel/8;
}

void  
Image::freePixelData()
{
    if (mp_ucPixelData)
    {
        delete[] mp_ucPixelData;
        mp_ucPixelData = NULL;
    }
}

void  
Image::allocatePixelData(int iWidth, int iHeight, int iBitPerPixel)
{
    if (iWidth <= 0 || iHeight <= 0 || iBitPerPixel % 8 != 0)
    {
        cout << "Can not allocate Pixels for Image(" << iWidth << "x" << iHeight << ":" << iBitPerPixel << ")" << endl;
        return;
    }
    if (mp_ucPixelData)
        freePixelData();
    
    mp_ucPixelData = new unsigned char[iWidth*iHeight*iBitPerPixel/8];
    if (mp_ucPixelData)
    {
        m_iWidth = iWidth;
        m_iHeight = iHeight;
        m_iBitPerPixel = iBitPerPixel;
    }
}

ImagePtr
Image::fromFile(const string& strFilename)
{
    FREE_IMAGE_FORMAT format = FIF_UNKNOWN;
    FIBITMAP* p_bitmap( NULL );
    int iBitPerPixel( 0 );
    int iWidth( 0 );
    int iHeight( 0 );

    format = FreeImage_GetFileType(strFilename.c_str(), 0);

    if (format == FIF_UNKNOWN)
      format = FreeImage_GetFIFFromFilename(strFilename.c_str());

    if (format == FIF_UNKNOWN) return ImagePtr();

    if (!FreeImage_FIFSupportsReading(format))
    {
      cout  << "Unsupported image type! Can not load image from file '" << strFilename << "'!" << endl;
      return ImagePtr();
    }
    

    p_bitmap = FreeImage_Load(format, strFilename.c_str());
    
    if (!p_bitmap)
    {
      cout << "Could not load Image '" << strFilename << "'!" << endl;
      return ImagePtr();
    }

    iWidth = FreeImage_GetWidth(p_bitmap);
    iHeight = FreeImage_GetHeight(p_bitmap);
    iBitPerPixel = FreeImage_GetBPP(p_bitmap); // Bit Per Pixel
    
    if (iHeight == 0 || iWidth == 0)
    {
      cout << "Image data error: " << strFilename << endl;
      FreeImage_Unload(p_bitmap);
      return ImagePtr();
    }

    ImagePtr p_img(new Image());
    p_img->loadPixels(FreeImage_GetBits(p_bitmap), iWidth, iHeight, iBitPerPixel);

    FreeImage_Unload(p_bitmap);
    return p_img;
}

void
Image::loadPixels(const unsigned char* p_pixels, int width, int height, int iBitPerPixel)
{
    allocatePixelData(width, height, iBitPerPixel);
    memcpy(mp_ucPixelData, p_pixels, width*height*iBitPerPixel/8);
}

const unsigned char* 
Image::getPixelData() const
{
    return mp_ucPixelData;
}