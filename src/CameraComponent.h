/* 
 * File:   CameraComponent.h
 * Author: Freddi
 *
 * Created on 8. August 2013, 22:44
 */

#ifndef CAMERACOMPONENT_H
#define	CAMERACOMPONENT_H

#include "Component.h"

class Camera;
typedef boost::shared_ptr<Camera> CameraPtr;

class CameraComponent;
typedef boost::shared_ptr<CameraComponent> CameraComponentPtr;

class CameraComponent : public Component {
public:
    CameraComponent();
    virtual ~CameraComponent();
private:
    CameraPtr mp_camera;
};

#endif	/* CAMERACOMPONENT_H */

