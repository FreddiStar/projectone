/* 
 * File:   Mathf.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 22:13
 */

#ifndef MATHF_H
#define	MATHF_H

class Mathf {
public:
    
    static float sqrt(float f);
    
    static float pow(float f, float power);
    
    static float cos(float f);
    static float sin(float f);
    static float tan(float f);
    
    static float deg2rad(float d);
    static float rad2deg(float r);
    
    Mathf() { }
    virtual ~Mathf() { };

};

#endif	/* MATHF_H */

