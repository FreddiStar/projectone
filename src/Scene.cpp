/* 
 * File:   Scene.cpp
 * Author: Freddi
 * 
 * Created on 8. August 2013, 21:17
 */

#include "Scene.h"
#include "RendererComponent.h"

Scene::Scene()
  : mp_sceneRoot( Instantiate<GameObject>() )
{
}

Scene::~Scene() {
}

void 
Scene::update()
{
    if (mp_sceneRoot)
        mp_sceneRoot->update();
}

void 
Scene::draw()
{
    //cout << "Scene::draw" << endl;
    // Bruteforce-Draw for now.
    // @todo Optimise with Culling (PVS)
    drawAll(mp_sceneRoot);
//    list<GameObjectPtr> listVisibleSet;
//    list<GameObjectPtr>::iterator it;
    
//    getVisibleSetDepthFirst(p_camera, listVisibleSet);
//    
//    for (it = listVisibleSet.begin(); it != listVisibleSet.end(); ++it)
//    {
//        Renderer::get().draw(*it);
//    }
}

void
Scene::drawAll(const GameObjectPtr& p_obj)
{
    glCheckError("Scene::drawAll");
    if (p_obj)
    {
        if (!p_obj->getTransformation()->getChildren().empty())
        {
            Transformation::TransformationPtrList::const_iterator it;
            for (it = p_obj->getTransformation()->getChildren().begin();
                    it != p_obj->getTransformation()->getChildren().end(); ++it)
            {
                drawAll((*it)->getGameObject());
            }
        }
        
        if (p_obj->getRenderer())
        {
            p_obj->getRenderer()->draw();
        }
    }
}

void
Scene::getVisibleSetDepthFirst(const CameraPtr& p_camera, list<GameObjectPtr>& r_listVisibleSet)
{

}

void
Scene::addGameObject(const GameObjectPtr& p_obj)
{
    if (mp_sceneRoot && p_obj)
    {
        mp_sceneRoot->addChild(p_obj);
    }
}