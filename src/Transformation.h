/* 
 * File:   Transformation.h
 * Author: frederiktrojahn
 *
 * Created on 2. August 2013, 18:45
 */

#ifndef TRANSFORMATION_H
#define	TRANSFORMATION_H

#include <list>

#include "Component.h"
#include "Types.h"

class Transformation;
typedef boost::shared_ptr<Transformation> TransformationPtr;
typedef boost::weak_ptr<Transformation> TransformationWPtr;

class Transformation : public Component
{
public:
    Transformation( );
    virtual ~Transformation( );

    typedef list<TransformationPtr> TransformationPtrList;

    void lookAt( const Vector3& at );

    Vector3 localUp( );
    Vector3 localForward( );
    Vector3 localRight( );
    
    Vector3 up( );
    Vector3 forward( );
    Vector3 right( );

    TransformationPtr root( );
    TransformationPtr parent( );

    Matrix4x4 localToGlobalMatrix( );
//    Matrix4x4 globalToLocalMatrix( ) const;


    Vector3& position( );
    Quaternion& rotation( );
//    Matrix4x4& rotation( );
    Vector3& scale( );

    void rotate( float fAngle, const Vector3& axis );

//    void update( );

    const TransformationPtrList& getChildren( ) const;

    Matrix4x4 getModelMatrix( );
    
    const Matrix4x4& getMatrix();
    
    Vector3 transformPosition(const Vector3& pos);
    Vector3 transformDirection(const Vector3& dir);

private:

    TransformationWPtr mwp_parent;

    TransformationPtrList m_listChildren;

    bool m_bTransformChanged;
    
    Matrix4x4 m_matTransformation;
    
    Vector3 m_vec3Position;
    Quaternion m_qatRotation;
//    Matrix4x4 m_matRotation;
    Vector3 m_vec3Scale;

    void updateTransformationMatrix( );

    // Internal use only
public:
    void addChild( const TransformationPtr& p_transform );
    void removeChild( const TransformationPtr& p_transform );
};

#endif	/* TRANSFORMATION_H */

