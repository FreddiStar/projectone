/* 
 * File:   Mesh.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 13:31
 */

//#include <SDL2/SDL_opengl.h>

#include "Mesh.h"
#include "Shader.h"
#include "Utils.h"

#define BUFFER_OFFSET(i) (reinterpret_cast<void*>(i))

Mesh::Mesh( GLenum eDrawMode )
    : m_uiBufferID( 0 )
    , m_uiIndexBufferID( 0 )

    , mp_indicesAttribute( new MeshAttributeGLuintGLuint( GL_ELEMENT_ARRAY_BUFFER, 0, GL_UNSIGNED_INT, 1 ) )

    , mp_verticesAttribute( new MeshAttributeVector3Float( GL_ARRAY_BUFFER, ShaderVariables::AttributeLocationVertex, GL_FLOAT, 3 ) )
    , mp_normalsAttribute( new MeshAttributeVector3Float( GL_ARRAY_BUFFER, ShaderVariables::AttributeLocationNormal, GL_FLOAT, 3 ) )
    , mp_uvAttribute( new MeshAttributeVector2Float( GL_ARRAY_BUFFER, ShaderVariables::AttributeLocationUV, GL_FLOAT, 2 ) )
    , mp_colorAttribute( new MeshAttributeColorFloat( GL_ARRAY_BUFFER, ShaderVariables::AttributeLocationColor, GL_FLOAT, 4 ) )

    , m_eDrawMode( eDrawMode )
{
    m_vecAttributes.push_back( mp_verticesAttribute );
    m_vecAttributes.push_back( mp_normalsAttribute );
    m_vecAttributes.push_back( mp_uvAttribute );
    m_vecAttributes.push_back( mp_colorAttribute );
}

Mesh::~Mesh( )
{
    m_vecAttributes.clear( );

    deleteVBO( );
}

const MeshAttributeGLuintGLuintPtr& 
Mesh::getIndexData()
{
    return mp_indicesAttribute;
}

const MeshAttributeVector3FloatPtr&
Mesh::getVertexData( ) 
{
    return mp_verticesAttribute;
}

const MeshAttributeVector3FloatPtr&
Mesh::getNormalData( )
{
    return mp_normalsAttribute;
}

const MeshAttributeVector2FloatPtr&
Mesh::getUVData( ) 
{
    return mp_uvAttribute;
}

const MeshAttributeColorFloatPtr&
Mesh::getColorData( ) 
{
    return mp_colorAttribute;
}

void
Mesh::setEnableVertices( bool b )
{
    if ( mp_verticesAttribute )
        mp_verticesAttribute->bEnabled = b;
}

void
Mesh::setEnableNormals( bool b )
{
    if ( mp_normalsAttribute )
        mp_normalsAttribute->bEnabled = b;
}

void
Mesh::setEnableUV( bool b )
{
    if ( mp_uvAttribute )
        mp_uvAttribute->bEnabled = b;
}

void
Mesh::setEnableColors( bool b )
{
    if ( mp_colorAttribute )
        mp_colorAttribute->bEnabled = b;
}

bool
Mesh::isDataChanged( ) const
{
    vector<MeshAttributeAbstractPtr>::const_iterator it;
    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
        if ( (*it)->isDataChanged( ) ) return true;

    return false;
}

void
Mesh::updateData( )
{
    vector<MeshAttributeAbstractPtr>::iterator it;
    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
        ( *it )->updateData( );

    mp_indicesAttribute->updateData( );
}

void
Mesh::pushVertex( const Vector3& vertex )
{
    mp_verticesAttribute->pushData( vertex );
}

void
Mesh::pushNormal( const Vector3& normal )
{
    mp_normalsAttribute->pushData( normal );
}

void
Mesh::pushIndex( GLuint index )
{
    mp_indicesAttribute->pushData( index );
}

void
Mesh::pushUV( const Vector2& uv )
{
    mp_uvAttribute->pushData( uv );
}

void
Mesh::pushColor( const Color& c )
{
    mp_colorAttribute->pushData( c );
}

bool
Mesh::isVertexDataEnabled( ) const
{
    return mp_verticesAttribute->bEnabled;
}

bool
Mesh::isNormalDataEnabled( ) const
{
    return mp_normalsAttribute->bEnabled;
}

bool
Mesh::isUVDataEnabled( ) const
{
    return mp_uvAttribute->bEnabled;
}

bool
Mesh::isColorDataEnabled( ) const
{
    return mp_indicesAttribute->bEnabled;
}

void
Mesh::createVBO( )
{
    glGenBuffers( 1, &m_uiBufferID );
    glGenBuffers( 1, &m_uiIndexBufferID );

    glCheckError( "Mesh::createVBO" );
}

void
Mesh::deleteVBO( )
{
    glDeleteBuffers( 1, &m_uiBufferID );
    glDeleteBuffers( 1, &m_uiIndexBufferID );

    glCheckError( "Mesh::deleteVBO" );
}

void
Mesh::updateVBO( )
{
    updateData( );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_uiIndexBufferID );

    glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                  mp_indicesAttribute->getDataSize( ),
                  mp_indicesAttribute->getData( ),
                  GL_STATIC_DRAW );

    vector<MeshAttributeAbstractPtr>::iterator it;

    int iElementCount = 0;
    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
    {
        if ( (*it)->bEnabled )
            iElementCount += (*it)->getDataSize( );
    }

    int iOffset = 0;

    glBindBuffer( GL_ARRAY_BUFFER, m_uiBufferID );
    glBufferData( GL_ARRAY_BUFFER, iElementCount, 0, GL_STATIC_DRAW );

    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
    {
        if ( (*it)->bEnabled )
        {
            glBufferSubData(
                             (*it)->enumTarget,
                             iOffset,
                             (*it)->getDataSize( ),
                             (*it)->getData( ) );
            (*it)->iOffset = iOffset;

            iOffset += (*it)->getDataSize( );
        }
    }
}

void
Mesh::enableAttributes( )
{
    vector<MeshAttributeAbstractPtr>::iterator it;
    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
    {
        MeshAttributeAbstractPtr p_attrib = *it;
        if ( p_attrib->bEnabled )
        {
            glEnableVertexAttribArray( p_attrib->iAttributeIndex );
            glVertexAttribPointer( p_attrib->iAttributeIndex,
                                   p_attrib->iElements,
                                   p_attrib->enumType,
                                   GL_FALSE,
                                   0,
                                   BUFFER_OFFSET( p_attrib->iOffset ) );
        }
    }

    //    if (isVertexDataEnabled())
    //    {
    //        glEnableVertexAttribArray(ShaderVariables::AttributeLocationVertex);
    //        glVertexAttribPointer(ShaderVariables::AttributeLocationVertex, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(m_iVertexDataOffset));
    //        glCheckError("AttributeLocationVertex");
    //    }

    glCheckError( "Mesh::enableAttributes" );
}

void
Mesh::disableAttributes( )
{
    vector<MeshAttributeAbstractPtr>::iterator it;
    for ( it = m_vecAttributes.begin( ); it != m_vecAttributes.end( ); ++it )
    {
        glDisableVertexAttribArray( (*it)->iAttributeIndex );
    }

    glCheckError( "Mesh::disableAttributes" );
}

void
Mesh::draw( )
{
    glCheckError( "Mesh::draw" );
    if ( !m_uiBufferID && !m_uiIndexBufferID )
    {
        createVBO( );
        updateVBO( );
    }

    if ( isDataChanged( ) )
    {
        deleteVBO( );
        createVBO( );
        updateVBO( );
    }

    bindVBO( );
    enableAttributes( );

    glDrawElements( m_eDrawMode, mp_indicesAttribute->getDataCount( ), mp_indicesAttribute->enumType, BUFFER_OFFSET( 0 ) );
    glCheckError( "glDrawElements" );

    disableAttributes( );
    unbindVBO( );
}

void
Mesh::bindVBO( )
{
    glBindBuffer( GL_ARRAY_BUFFER, m_uiBufferID );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_uiIndexBufferID );

    glCheckError( "Mesh::bindVBO" );
}

void
Mesh::unbindVBO( )
{
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );

    glCheckError( "Mesh::unbindVBO" );
}