/* 
 * File:   GameObject.h
 * Author: frederiktrojahn
 *
 * Created on 2. August 2013, 17:43
 */

#ifndef GAMEOBJECT_H
#define	GAMEOBJECT_H

#include <list>

#include "Component.h"
#include "Transformation.h"

#include "boost/enable_shared_from_this.hpp"
#include "LightComponent.h"
#include "CameraComponent.h"
#include "RendererComponent.h"
#include "MeshComponent.h"

using namespace std;

template<class T>
boost::shared_ptr<T> Instantiate()
{
    boost::shared_ptr<T> obj(new T);
    obj->init();
    
    return obj;
}

class GameObject : public boost::enable_shared_from_this<GameObject> 
{
public:
    GameObject();
    virtual ~GameObject();
    
    virtual void init();
    
    GameObjectPtr getThis() { return shared_from_this(); }
        
    const TransformationPtr& getTransformation();
    const RendererComponentPtr& getRenderer();
    
    void setRenderer(const RendererComponentPtr& p_comRenderer);
    void setMesh(const MeshComponentPtr& p_compMesh);
    
    void addComponent(const ComponentPtr& p_comp);
    
    template<typename T>
    boost::shared_ptr<T> getComponent()
    {
        list<ComponentPtr>::iterator it;
        for (it = m_listComponents.begin(); it != m_listComponents.end(); ++it)
        {
            boost::shared_ptr<T> p_comp = boost::dynamic_pointer_cast<T>(*it);
            if (p_comp)
                return p_comp;
        }
        
        return boost::shared_ptr<T>();
    }
    
    template<typename T>
    void removeComponent()
    {
        list<ComponentPtr>::iterator it;
        for (it = m_listComponents.begin(); it != m_listComponents.end(); ++it)
        {
            boost::shared_ptr<T> p_comp = boost::dynamic_pointer_cast<T>(*it);
            if (p_comp)
            {
                it = m_listComponents.erase(it);
                return;
            }
        }
    }
    
    virtual void update();
    
    void addChild(const GameObjectPtr& p_obj);
    void removeChild(const GameObjectPtr& p_obj);
    bool hasChild(const GameObjectPtr& p_obj);
    
private:
    list<ComponentPtr>      m_listComponents;
    
    TransformationPtr       mp_transformation;
    
    list<GameObjectPtr>     m_listChildren;
    
    RendererComponentPtr    mp_compRenderer;
    MeshComponentPtr        mp_compMesh;
    //LightComponentPtr       mp_compLight;
    //CameraComponentPtr      mp_compCamera;
    
    void addComponentToList(const ComponentPtr& p_comp);
};

#endif	/* GAMEOBJECT_H */

