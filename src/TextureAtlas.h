/* 
 * File:   TextureAtlas.h
 * Author: Freddi
 *
 * Created on 28. Januar 2014, 14:39
 */

#ifndef TEXTUREATLAS_H
#define	TEXTUREATLAS_H

#include "boost/shared_ptr.hpp"
#include "Image.h"
#include "Texture2D.h"
#include "TextureAtlasRect.h"
#include <string>
#include <map>

using namespace std;

class TextureAtlas;
typedef boost::shared_ptr<TextureAtlas> TextureAtlasPtr;

enum TextureAtlasUVCoordinateName
{
    UVCoordinateTopLeft = 0,
    UVCoordinateBottomLeft = 1,
    UVCoordinateBottomRight = 2,
    UVCoordinateTopRight = 3,
    UVCoordinateCount = 4
};

struct TextureAtlasLocation
{
public:

    TextureAtlasLocation( string strID,
                          uint32_t uiAtlasWidth,
                          uint32_t uiAtlasHeight,
                          uint32_t uiLeft,
                          uint32_t uiRight,
                          uint32_t uiTop,
                          uint32_t uiBottom )
        : p_rectTile( new TextureAtlasRect( uiAtlasWidth, uiAtlasHeight, uiLeft, uiRight, uiTop, uiBottom ) )
        , strTileID( strID )
    {
    }
    TextureAtlasRectPtr p_rectTile;
    string strTileID;
};

typedef boost::shared_ptr<TextureAtlasLocation> TextureAtlasLocationPtr;

class TextureAtlas
{
public:
    TextureAtlas( );
    virtual ~TextureAtlas( );

    bool load( const string& c_strFile );

    TextureAtlasLocationPtr getByID( const string& c_strID );

private:
    string m_strFilename;
    ImagePtr mp_imgAtlas;
    Texture2DPtr mp_texAtlas;

    map<string, TextureAtlasLocationPtr> m_mapIDToLocation;
};

#endif	/* TEXTUREATLAS_H */

