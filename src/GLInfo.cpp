#include "GLInfo.h"

#include "OpenGL.h"
#include <iostream>

GLInfo* GLInfo::smp_glInfo = NULL;

const GLInfo& GLInfo::get()
{
  if (!smp_glInfo)
  {
    smp_glInfo = new GLInfo();
  }

  return *smp_glInfo;
}

GLInfo::GLInfo()
{
}

void GLInfo::printOpenGLInfo() const
{
  cout << glGetString(GL_VENDOR) << endl;
  cout << glGetString(GL_RENDERER) << endl;
  cout << "OpenGL " << glGetString(GL_VERSION) << endl;
  cout << "GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl << endl;

  int i;
  glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &i);
  cout << "GL_MAX_COLOR_ATTACHMENTS: " << i << endl;

  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &i);
  cout << "GL_MAX_TEXTURE_SIZE: " << i << "x" << i << endl;

  glGetIntegerv(GL_MAX_DRAW_BUFFERS, &i);
  cout << "GL_MAX_DRAW_BUFFERS: " << i << endl;

  glGetIntegerv(GL_MAX_TEXTURE_UNITS, &i);
  cout << "GL_MAX_TEXTURE_UNITS: " << i << endl;

  glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &i);
  cout << "GL_MAX_TEXTURE_BUFFER_SIZE: " << i << endl;

  glGetIntegerv(GL_MAX_TEXTURE_COORDS, &i);
  cout << "GL_MAX_TEXTURE_COORDS: " << i << endl;

  glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &i);
  cout << "GL_MAX_UNIFORM_BLOCK_SIZE: " << i << endl;

  glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &i);
  cout << "GL_MAX_UNIFORM_LOCATIONS: " << i << endl;

  glGetIntegerv(GL_MAX_SAMPLES, &i);
  cout << "GL_MAX_SAMPLES: " << i << endl;

  glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &i);
  cout << "GL_MAX_RENDERBUFFER_SIZE: " << i << endl;

  glGetIntegerv(GL_MAX_VARYING_COMPONENTS, &i);
  cout << "GL_MAX_VARYING_COMPONENTS: " << i << endl;

  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &i);
  cout << "GL_MAX_VERTEX_ATTRIBS: " << i << endl;
}

GLInfo::~GLInfo()
{
  //dtor
}