/* 
 * File:   Application.cpp
 * Author: frederiktrojahn
 * 
 * Created on 30. Juli 2013, 22:30
 */

#include <SDL2/SDL_video.h>


#include "Application.h"
#include "OpenGL.h"
#include "Time.h"
#include "config.h"
#include "Types.h"
#include "FreeImage.h"
#include "DiffuseColorMaterial.h"
#include "SceneManager.h"
#include "TestScene.h"
#include "GLInfo.h"
#include "InputManager.h"
#include "InputListener.h"
#include "LuaState.h"

Application::Application( )
    : mp_window( NULL )
    , m_glContext( )
    , m_bRunning( true )
    , m_uiLastTime( )
    , m_uiFrameMS( 16 ) { }

Application::~Application( ) { }

bool Application::init( int argc, char** argv )
{
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        return false;
    }

    FreeImage_Initialise( true );

    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );

    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

#ifdef GL_ERROR_CHECKING
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG );
#endif

    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );

    mp_window = SDL_CreateWindow( "ProjectOne", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    //    cout << "SDL Error SDL_CreateWindow: " << SDL_GetError() << endl;

    if ( !mp_window )
    {
        return false;
    }

    m_glContext = SDL_GL_CreateContext( mp_window );

    SDL_GL_MakeCurrent( mp_window, m_glContext );
    //    cout << "SDL Error SDL_GL_CreateContext: " << SDL_GetError() << endl;
    glCheckError( "SDL_GL_CreateContext" );
    if ( glewInit( ) != GLEW_OK )
    {
        _VERBOSE_LOG( "Could not initialize GLEW" );
        return false;
    }
    glCheckError( "glewInit" );

    //    if (GLEW_VERSION_2_0) { cout << "GLEW_VERSION_2_0" << endl; }
    //    if (GLEW_VERSION_2_1) { cout << "GLEW_VERSION_2_1" << endl; }
    //    if (GLEW_VERSION_3_0) { cout << "GLEW_VERSION_3_0" << endl; }
    //    if (GLEW_VERSION_3_1) { cout << "GLEW_VERSION_3_1" << endl; }
    //    if (GLEW_VERSION_3_2) { cout << "GLEW_VERSION_3_2" << endl; }
    //    if (GLEW_VERSION_3_3) { cout << "GLEW_VERSION_3_3" << endl; }
    //    if (GLEW_VERSION_4_0) { cout << "GLEW_VERSION_4_0" << endl; }
    //    if (GLEW_VERSION_4_1) { cout << "GLEW_VERSION_4_1" << endl; }
    //    if (GLEW_VERSION_4_2) { cout << "GLEW_VERSION_4_2" << endl; }
    //    if (GLEW_VERSION_4_3) { cout << "GLEW_VERSION_4_3" << endl; }
    //    if (GLEW_VERSION_4_4) { cout << "GLEW_VERSION_4_4" << endl; }

    GLInfo::get( ).printOpenGLInfo( );
    glCheckError( "Application::init" );

    //    glEnableClientState(GL_INDEX_ARRAY);
    //    glEnableClientState(GL_VERTEX_ARRAY);

    glCullFace( GL_FRONT_AND_BACK );

    SceneManager::get( ).setScene( ScenePtr( new TestScene( ) ) );

    return true;
}

void Application::onEvent( const SDL_Event& event )
{
    switch ( event.type )
    {
        case SDL_QUIT: close( );
            break;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
        {
            onKeyboard( event.key.keysym.sym,
                        event.key.state,
                        event.key.keysym.mod );
            break;
        }
    }
}

void Application::onKeyboard( const SDL_Keycode& key, Uint8 keyState, Uint16 keyMod )
{
    KeyboardInputArgs args;
    args.key = key;
    if ( keyState == SDL_PRESSED )
    {
        if ( InputManager::get( )->onKeyPress( args ) ) return;
    }
    else
    {
        if ( InputManager::get( )->onKeyRelease( args ) ) return;
    }

    switch ( key )
    {
        case SDLK_ESCAPE: close( );
            break;
    }
}

void Application::run( )
{
    SDL_Event event;

    while ( m_bRunning )
    {
        while ( SDL_PollEvent( &event ) )
        {
            onEvent( event );
        }

        update( );
        draw( );
    }
}

void Application::close( )
{
    m_bRunning = false;
}

void Application::quit( )
{
    // Singletones löschen
    SceneManager::destroy( );
    InputManager::destory( );
    LuaState::destory( );

    SDL_GL_DeleteContext( m_glContext );
    SDL_DestroyWindow( mp_window );

    FreeImage_DeInitialise( );

    SDL_Quit( );
}

void Application::update( )
{
    Uint32 uiUpdateTime = SDL_GetTicks( );

    Time::sm_fDeltaTime = ((SDL_GetTicks( ) - m_uiLastTime)*0.001f);

    // Der AppUpdate hier
    if ( SceneManager::get( ).getScene( ) )
    {
        SceneManager::get( ).getScene( )->update( );
    }

    // Restart FrameTime
    m_uiLastTime = SDL_GetTicks( );

    // Wie es aussieht Limitiert SDL die FPS selber. Allerdings nur
    // Wenn die Anwendung sichtbar ist.
    uiUpdateTime = SDL_GetTicks( ) - uiUpdateTime;
    if ( uiUpdateTime < m_uiFrameMS ) // Limit to ~60 FPS
    {
        SDL_Delay( m_uiFrameMS - uiUpdateTime );

    }
}

void Application::draw( )
{
    glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

    glViewport( 0, 0, 640, 480 );

    if ( SceneManager::get( ).getScene( ) )
    {
        SceneManager::get( ).getScene( )->draw( );
    }

    SDL_GL_SwapWindow( mp_window );

    glCheckError( "Frame Ende" );
}