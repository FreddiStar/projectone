/* 
 * File:   Application.h
 * Author: frederiktrojahn
 *
 * Created on 30. Juli 2013, 22:30
 */

#ifndef APPLICATION_H
#define	APPLICATION_H


#include "OpenGL.h"

class Application
{
public:
    Application( );
    virtual ~Application( );

    // Inits the Application
    virtual bool init( int argc, char** argv );

    // Starts the Mainloop
    virtual void run( );

    // Stopps the Mainloop
    virtual void close( );

    // Destories all SDL Objects an does Cleanup
    virtual void quit( );

    // Update. Called once per Frame
    virtual void update( );

    // Draws the Frame
    virtual void draw( );

    // Event-Callback for SDL
    virtual void onEvent( const SDL_Event& event );

    // Keyboardevents
    virtual void onKeyboard( const SDL_Keycode& key, Uint8 keyState, Uint16 keyMod );
private:
    SDL_Window* mp_window;
    SDL_GLContext m_glContext;

    bool m_bRunning;

    Uint32 m_uiLastTime;
    Uint32 m_uiFrameMS;
};

#endif	/* APPLICATION_H */

