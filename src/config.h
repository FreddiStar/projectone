/* 
 * File:   config.h
 * Author: Freddi
 *
 * Created on 27. August 2013, 20:13
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif

#define ENABLE_ASSERTS
#define VERBOSE_LOGGING
#undef OPENGL_DEBUG_CALLBACK
    
#define GL_ERROR_CHECKING
    
#ifdef ENABLE_ASSERTS
    #include "assert.h"
    #define _ASSERT(expr) assert((expr))
#else
    #define _ASSERT(expr) ((void)0)
#endif    

#ifdef VERBOSE_LOGGING
    #define _VERBOSE_LOG(msg) cout << msg << endl;
#else
    #define _VERBOSE_LOG(msg) ((void)0);
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

