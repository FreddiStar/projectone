/* 
 * File:   MeshComponent.cpp
 * Author: Freddi
 * 
 * Created on 8. August 2013, 20:51
 */

#include "MeshComponent.h"

MeshComponent::MeshComponent()
  : mp_mesh( )
{
}

MeshComponent::~MeshComponent() {
}

MeshPtr 
MeshComponent::getMesh()
{
    return mp_mesh;
}

void 
MeshComponent::setMesh(const MeshPtr& p_mesh)
{
    if (!p_mesh) return;
    mp_mesh = p_mesh;
}
