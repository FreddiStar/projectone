/* 
 * File:   Primitives.h
 * Author: Freddi
 *
 * Created on 29. August 2013, 22:23
 */

#ifndef PRIMITIVES_H
#define	PRIMITIVES_H

#include "Mesh.h"


class Primitives {
public:
    virtual ~Primitives();
    
    static MeshPtr Box();
    static MeshPtr Sphere();
    static MeshPtr Plane(int iSubdivisions = 0);
    static MeshPtr Cylinder(int iResolution = 0);
    
    static MeshPtr QuadsPlane(int w, int h);
    
    static MeshPtr Axis();
    
private:
    // Not needed
    Primitives(); 
    Primitives(const Primitives& orig);
};

#endif	/* PRIMITIVES_H */

