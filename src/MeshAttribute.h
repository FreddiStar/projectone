/* 
 * File:   MeshAttribute.h
 * Author: Freddi
 *
 * Created on 22. November 2013, 22:11
 */

#ifndef MESHATTRIBUTE_H
#define	MESHATTRIBUTE_H

#include <vector>

#include "Types.h"
#include "Utils.h"
using namespace std;

struct MeshAttributeAbstract;
typedef boost::shared_ptr<MeshAttributeAbstract> MeshAttributeAbstractPtr;

struct MeshAttributeAbstract
{  
    MeshAttributeAbstract( GLenum target, GLuint index, GLenum type, GLint elements )
        : enumTarget( target )
        , iDataSize( 0 )
    
        , iAttributeIndex( index )
        , iElements( elements )
        , enumType( type )
        , iStride( 0 )
        , iOffset( 0 )
    
        , bEnabled( false )
        , bDataChanged( true )
    {}
    GLenum      enumTarget;
    
    uint32_t    iDataSize;
    
    GLuint      iAttributeIndex;
    GLint       iElements;
    GLenum      enumType;
    GLsizei     iStride;
    uint32_t    iOffset;
    
    bool        bEnabled;
    
    virtual void        updateData() = 0;
    virtual void*       getData() = 0;
    virtual int         getDataSize() = 0;
    virtual size_t      sizeofDataType() = 0;
    virtual int         getDataCount() = 0;
    
    bool                isDataChanged() const { return bDataChanged; }

protected:
    bool        bDataChanged;
};

//struct MeshAttribute;
//typedef boost::shared_ptr<MeshAttribute> MeshAttributePtr;

//template<typename d, typename i, typename v>
//void toArray(d*& p_data, i& index, v&)

inline void toArray(GLuint* p_data, int& index, GLuint i)
{
    p_data[index++] = i;
}

inline void toArray(float* p_data, int& index, Vector3 v)
{
    p_data[index++] = v.x;
    p_data[index++] = v.y;
    p_data[index++] = v.z;
}

inline void toArray(float* p_data, int& index, Vector2 v)
{
    p_data[index++] = v.x;
    p_data[index++] = v.y;
}

inline void toArray(float* p_data, int& index, Color c)
{
    p_data[index++] = c.x;
    p_data[index++] = c.y;
    p_data[index++] = c.z;
    p_data[index++] = c.w;
}

template<typename VectorType, typename DataType>
struct MeshAttribute : public MeshAttributeAbstract
{
    MeshAttribute( GLenum target, GLuint index, GLenum type, GLint elements ) 
        : MeshAttributeAbstract( target, index, type, elements )
        , m_vecData()
        , p_data( NULL ) { };
    virtual ~MeshAttribute() { safeDelete(p_data); };
    
    typedef vector<VectorType> VectorT;
    
    void*       getData() { return static_cast<void*>(p_data); }
    void        updateData()
    {
        safeDeleteArray(p_data);
        
        p_data = new DataType[m_vecData.size()*iElements];
        typename VectorT::const_iterator it;
        int index = 0;
        for (it = m_vecData.begin(); it != m_vecData.end(); ++it)
        {
            toArray(p_data, index, *it);
        }
        
        bDataChanged = false;
    }
    
    int                 getDataSize() { return sizeof(DataType)*m_vecData.size()*iElements; }
    size_t              sizeofDataType() { return sizeof(DataType); }
    void                pushData(const VectorType& d) { m_vecData.push_back(d); bDataChanged = true; bEnabled = true; }
    int                 getDataCount() { return static_cast<int>(m_vecData.size()); }
    
private:
    vector<VectorType> m_vecData;
    DataType*   p_data;
};

typedef MeshAttribute<Vector3, float> MeshAttributeVector3Float;
typedef boost::shared_ptr<MeshAttributeVector3Float> MeshAttributeVector3FloatPtr;

typedef MeshAttribute<GLuint, GLuint> MeshAttributeGLuintGLuint;
typedef boost::shared_ptr<MeshAttributeGLuintGLuint> MeshAttributeGLuintGLuintPtr;

typedef MeshAttribute<Vector2, float> MeshAttributeVector2Float;
typedef boost::shared_ptr<MeshAttributeVector2Float> MeshAttributeVector2FloatPtr;

typedef MeshAttribute<Color, float> MeshAttributeColorFloat;
typedef boost::shared_ptr<MeshAttributeColorFloat> MeshAttributeColorFloatPtr;

#endif	/* MESHATTRIBUTE_H */

