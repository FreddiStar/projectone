/* 
 * File:   Texture.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 13:32
 */

#include "Texture.h"

Texture::Texture( )
    : m_uiTextureID( 0 ) { }

Texture::~Texture( ) { }

GLuint
Texture::getTexture( ) const
{
    return m_uiTextureID;
}
