/* 
 * File:   InputManager.cpp
 * Author: Freddi
 * 
 * Created on 20. November 2013, 21:30
 */

#include "InputManager.h"
#include "Application.h"
#include "InputListener.h"

InputManagerPtr InputManager::p_instance = InputManagerPtr();

InputManager::InputManager()
    : m_listKeyboardInput( )
    , m_listMouseInput( )
{
}

InputManager::InputManager(const InputManager& orig) {
}

InputManager::~InputManager() {
}

void InputManager::addListener(MouseInputListener* p_listener)
{
    list<MouseInputListener*>::const_iterator it;
    for (it = m_listMouseInput.begin(); it != m_listMouseInput.end(); ++it)
    {
        if (*it == p_listener) return;
    }
    
    m_listMouseInput.push_back(p_listener);
}

void InputManager::addListener(KeyboardInputListener* p_listener)
{
    list<KeyboardInputListener*>::const_iterator it;
    for (it = m_listKeyboardInput.begin(); it != m_listKeyboardInput.end(); ++it)
    {
        if (*it == p_listener) return;
    }
    
    m_listKeyboardInput.push_back(p_listener);
}

void InputManager::removeListener(MouseInputListener* p_listener)
{
    m_listMouseInput.remove(p_listener);
}

void InputManager::removeListener(KeyboardInputListener* p_listener)
{
    m_listKeyboardInput.remove(p_listener);
}

InputManagerPtr InputManager::get()
{
    if (!p_instance)
    {
        p_instance.reset(new InputManager());
    }
    
    return p_instance;
}

void InputManager::destory()
{
    p_instance.reset();
}

bool InputManager::onKeyPress(KeyboardInputArgs& args)
{
    list<KeyboardInputListener*>::const_iterator it;
    for (it = m_listKeyboardInput.begin(); it != m_listKeyboardInput.end(); ++it)
    {
        if ((*it)->onKeyPress(args)) return true;
    }
    
    return false;
}

bool InputManager::onKeyRelease(KeyboardInputArgs& args)
{
    list<KeyboardInputListener*>::const_iterator it;
    for (it = m_listKeyboardInput.begin(); it != m_listKeyboardInput.end(); ++it)
    {
        if ((*it)->onKeyRelease(args)) return true;
    }
    
    return false;
}