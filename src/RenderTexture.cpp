/* 
 * File:   RenderTexture.cpp
 * Author: Freddi
 * 
 * Created on 4. August 2013, 21:44
 */

#include "RenderTexture.h"

RenderTexture::RenderTexture( )
    : Texture( )
{
    glGenFramebuffers( 1, &m_uiTextureID );
}

RenderTexture::~RenderTexture( ) {
 }

void RenderTexture::begin( ) {
 }

void RenderTexture::end( ) {
 }