/* 
 * File:   AnimatedMesh.h
 * Author: Freddi
 *
 * Created on 24. November 2013, 14:15
 */

#ifndef ANIMATEDMESH_H
#define	ANIMATEDMESH_H

#include "Mesh.h"

class AnimatedMesh;
typedef boost::shared_ptr<AnimatedMesh> AnimatedMeshPtr;

class AnimatedMesh : public Mesh {
public:
    AnimatedMesh();
    virtual ~AnimatedMesh();
private:

};

#endif	/* ANIMATEDMESH_H */

