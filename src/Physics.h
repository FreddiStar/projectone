/* 
 * File:   Physics.h
 * Author: Freddi
 *
 * Created on 13. Januar 2014, 22:19
 */

#ifndef PHYSICS_H
#define	PHYSICS_H

#include <btBulletDynamicsCommon.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <boost/shared_ptr.hpp>

using namespace boost;

typedef shared_ptr<btBroadphaseInterface> btBroadphaseInterfacePtr;
typedef shared_ptr<btRigidBody> btRigidBodyPtr;

typedef shared_ptr<btSphereShape> btSphereShapePtr;

inline btSphereShapePtr createSphere()
{
    return btSphereShapePtr(new btSphereShape(btScalar(1.0f)));
}

#endif	/* PHYSICS_H */

