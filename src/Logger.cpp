/* 
 * File:   Logger.cpp
 * Author: Freddi
 * 
 * Created on 29. Januar 2014, 21:33
 */

#include "Logger.h"
#include "Utils.h"

Logger* Logger::smp_singleton = NULL;

Logger::Logger( ) { }

Logger::~Logger( ) { }

Logger& Logger::get( )
{
    if ( !smp_singleton )
        smp_singleton = new Logger( );

    return *smp_singleton;
}

void Logger::destory( )
{
    safeDelete( smp_singleton );
}

