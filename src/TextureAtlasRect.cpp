/* 
 * File:   TextureAtlasRect.cpp
 * Author: Freddi
 * 
 * Created on 28. Januar 2014, 15:12
 */

#include "TextureAtlasRect.h"
#include "Mathf.h"
#include "Utils.h"

TextureAtlasRect::TextureAtlasRect( uint32_t uiAtlasWidth,
                                    uint32_t uiAtlasHeight,
                                    uint32_t uiLeft,
                                    uint32_t uiRight,
                                    uint32_t uiTop,
                                    uint32_t uiBottom )
    : m_fLeft( 0.0f )
    , m_fRight( 1.0f )
    , m_fTop( 0.0f )
    , m_fBottom( 1.0f )

    , m_uiWidth( 0 )
    , m_uiHeight( 0 )

    , m_uiAtlasWidth( uiAtlasWidth )
    , m_uiAtlasHeight( uiAtlasHeight )

    , m_uiLeft( Math::min( uiLeft, uiRight ) )
    , m_uiRight( Math::max( uiLeft, uiRight ) )
    , m_uiTop( Math::min( uiTop, uiBottom ) )
    , m_uiBottom( Math::max( uiTop, uiBottom ) )
{
    m_uiWidth = m_uiRight - m_uiLeft;
    m_uiHeight = m_uiBottom - m_uiTop;

    float fWidthRatio = static_cast<float>(m_uiWidth) / static_cast<float>(m_uiAtlasWidth);
    float fHeightRatio = static_cast<float>(m_uiHeight) / static_cast<float>(m_uiAtlasHeight);

    m_fLeft = static_cast<float>(m_uiLeft) * fWidthRatio;
    m_fRight = static_cast<float>(m_uiRight) * fWidthRatio;

    m_fTop = static_cast<float>(m_uiTop) * fHeightRatio;
    m_fBottom = static_cast<float>(m_uiBottom) * fHeightRatio;
}

TextureAtlasRect::~TextureAtlasRect( ) { }

Vector2 TextureAtlasRect::getTopLeft() const
{
    return Vector2(m_fLeft, m_fTop);
}

Vector2 TextureAtlasRect::getTopRight() const
{
    return Vector2(m_fRight, m_fTop);
}

Vector2 TextureAtlasRect::getBottomLeft() const
{
    return Vector2(m_fLeft, m_fBottom);
}

Vector2 TextureAtlasRect::getBottomRight() const
{
    return Vector2(m_fRight, m_fBottom);
}

