/* 
 * File:   Ray.cpp
 * Author: Freddi
 * 
 * Created on 21. August 2013, 20:26
 */

#include "Ray.h"

Ray::Ray(const Vector3& vecPosition, const Vector3& vecDirection)
    : m_vecPosition( vecPosition )
    , m_vecDirection( vecDirection )
{
}

Ray::~Ray() {
}

