/* 
 * File:   GameObject.cpp
 * Author: frederiktrojahn
 * 
 * Created on 2. August 2013, 17:43
 */

#include "MeshComponent.h"


#include "GameObject.h"
#include "RendererComponent.h"
#include "MeshRendererComponent.h"
#include "config.h"

GameObject::GameObject( )
    : m_listComponents( )
    , mp_transformation( new Transformation( ) )
    , mp_compRenderer( ) { }

void GameObject::init( )
{
    addComponent( mp_transformation );
}

GameObject::~GameObject( ) { }

const TransformationPtr&
GameObject::getTransformation( )
{
    return mp_transformation;
}

void
GameObject::addComponent( const ComponentPtr& p_comp )
{
    if ( !p_comp ) return;

    // cout << "addComponent " << typeid(*p_comp).name() << endl;

    if ( typeid (*p_comp.get( )) == typeid (MeshRendererComponent) )
    {
        setRenderer( boost::dynamic_pointer_cast<MeshRendererComponent>(p_comp) );
    }
    else if ( typeid (*p_comp.get( )) == typeid (MeshComponent) )
    {
        setMesh( boost::dynamic_pointer_cast<MeshComponent>(p_comp) );
    }
    else
        addComponentToList( p_comp );
}

void setMesh( const MeshComponentPtr& p_compMesh );

void
GameObject::addComponentToList( const ComponentPtr& p_comp )
{
    // cout << "addComponentToList " << typeid(*p_comp).name() << endl;
    m_listComponents.push_back( p_comp );
    p_comp->setGameObject( getThis( ) );
}

void
GameObject::update( )
{
    list<ComponentPtr>::iterator it;
    for ( it = m_listComponents.begin( ); it != m_listComponents.end( ); ++it )
    {
        if ( (*it)->isEnabled( ) )
            ( *it )->update( );
    }

    Transformation::TransformationPtrList::const_iterator itTrans;
    for ( itTrans = mp_transformation->getChildren( ).begin( );
          itTrans != mp_transformation->getChildren( ).end( );
          ++itTrans )
    {
        if ( (*itTrans)->getGameObject( ) )
            ( *itTrans )->getGameObject( )->update( );
    }

}

void
GameObject::addChild( const GameObjectPtr& p_obj )
{
    if ( !p_obj ) return;

    mp_transformation->addChild( p_obj->mp_transformation );

    if ( !hasChild( p_obj ) )
        m_listChildren.push_back( p_obj );
}

void
GameObject::removeChild( const GameObjectPtr& p_obj )
{
    if ( !p_obj ) return;

    mp_transformation->removeChild( p_obj->mp_transformation );

    list<GameObjectPtr>::iterator it;
    for ( it = m_listChildren.begin( ); it != m_listChildren.end( ); )
    {
        if ( (*it).get( ) == p_obj.get( ) )
            it = m_listChildren.erase( it );
        else
            ++it;
    }
}

bool
GameObject::hasChild( const GameObjectPtr& p_obj )
{
    list<GameObjectPtr>::const_iterator it;
    for ( it = m_listChildren.begin( ); it != m_listChildren.end( ); ++it )
    {
        if ( (*it).get( ) == p_obj.get( ) )
            return true;
    }

    return false;
}

const RendererComponentPtr&
GameObject::getRenderer( )
{
    return mp_compRenderer;
}

void
GameObject::setRenderer( const RendererComponentPtr& p_compRenderer )
{
    removeComponent<RendererComponent>();
    mp_compRenderer = p_compRenderer;
    if ( mp_compRenderer )
    {
        addComponentToList( mp_compRenderer );
    }

}

void
GameObject::setMesh( const MeshComponentPtr& p_compMesh )
{
    removeComponent<MeshComponent>();
    mp_compMesh = p_compMesh;
    if ( mp_compMesh )
    {
        addComponentToList( p_compMesh );
    }
}