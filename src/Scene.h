/* 
 * File:   Scene.h
 * Author: Freddi
 *
 * Created on 8. August 2013, 21:17
 */

#ifndef SCENE_H
#define	SCENE_H

#include "boost/shared_ptr.hpp"
#include "Component.h"
#include "GameObject.h"
#include "Camera.h"

class Scene;
typedef boost::shared_ptr<Scene> ScenePtr;

class Scene {
public:
    Scene();
    virtual ~Scene();
    
    virtual void setup() { };
    virtual void unload() { };
    
    virtual void update();
    virtual void draw();
    
    virtual void addGameObject(const GameObjectPtr& p_obj);
    
private:
    GameObjectPtr mp_sceneRoot;
    
    void getVisibleSetDepthFirst(const CameraPtr& p_camera, list<GameObjectPtr>& r_listVisibleSet);
    void drawAll(const GameObjectPtr& p_obj);
};

#endif	/* SCENE_H */

