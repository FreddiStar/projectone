/* 
 * File:   Material.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 13:34
 */

#ifndef MATERIAL_H
#define	MATERIAL_H

#include "boost/shared_ptr.hpp"
#include "Shader.h"
#include "Component.h"
#include "Mesh.h"

class Material;
typedef boost::shared_ptr<Material> MaterialPtr;

class Material {
public:
    Material();
    virtual ~Material();
    
    virtual bool setup();
    virtual bool loadShader() = 0;
    
    virtual void setUniforms(const GameObjectPtr& p_obj);
    virtual void draw(const GameObjectPtr& p_obj);
    
    virtual void getUniformsFromShader();
    
    const ShaderPtr& getShader();
    
protected:
    ShaderPtr   mp_shader;
    
    //Uniforms
    GLint       m_iShaderUniformMVP;
    
    // Textures
    
    // misc
    bool        m_bMeshGotAttributesFromShader;
};

#endif	/* MATERIAL_H */

