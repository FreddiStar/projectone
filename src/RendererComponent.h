/* 
 * File:   Renderer.h
 * Author: Freddi
 *
 * Created on 8. August 2013, 20:56
 */

#ifndef RENDERERCOMPONENT_H
#define	RENDERERCOMPONENT_H

#include "boost/shared_ptr.hpp"
#include "Component.h"
#include "Material.h"

class RendererComponent;
typedef boost::shared_ptr<RendererComponent> RendererComponentPtr;

class RendererComponent : public Component {
public:
    RendererComponent();
    virtual ~RendererComponent();
    
    virtual void draw() = 0;
    
    virtual void setMaterial(const MaterialPtr& p_mat);
    virtual const MaterialPtr& getMaterial();
protected:
    MaterialPtr mp_material;
};

#endif	/* RENDERERCOMPONENT_H */

