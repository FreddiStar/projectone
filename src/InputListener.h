/* 
 * File:   KeyboardInputListener.h
 * Author: Freddi
 *
 * Created on 20. November 2013, 21:22
 */

#ifndef KEYBOARDINPUTLISTENER_H
#define	KEYBOARDINPUTLISTENER_H

#include "InputManager.h"


struct KeyboardInputArgs
{
    KeyboardInputArgs() : key( 0 ), alt( false ), ctrl( false ), shift( false ) {}
    int key;
    bool alt, ctrl, shift;
};

struct MouseInputArgs
{
    int x, y;
    int button;
};

class KeyboardInputListener {
public:
    KeyboardInputListener() { InputManager::get()->addListener(this); };
    virtual ~KeyboardInputListener() { };
    
    virtual bool onKeyPress(KeyboardInputArgs& args) { return false; }
    virtual bool onKeyRelease(KeyboardInputArgs& args) { return false; }
private:

};

class MouseInputListener {
public:
    MouseInputListener() { InputManager::get()->addListener(this); }
    virtual ~MouseInputListener() { };
    
    virtual bool onMouseMove(MouseInputArgs& args) { return false; }
    virtual bool onMouseEnter(MouseInputArgs& args) { return false; }
    virtual bool onMouseLeave(MouseInputArgs& args) { return false; }
    virtual bool onMouseClick(MouseInputArgs& args) { return false; }
    virtual bool onMouseRelease(MouseInputArgs& args) { return false; }
};

#endif	/* KEYBOARDINPUTLISTENER_H */

