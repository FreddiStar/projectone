/* 
 * File:   FreeCamera.cpp
 * Author: Freddi
 * 
 * Created on 20. November 2013, 22:12
 */

#include "FreeCamera.h"
#include "Time.h"

FreeCamera::FreeCamera( )
    : Camera( )
    , KeyboardInputListener( )
    , MouseInputListener( )
    , m_fMovementSpeed( 10.0f )
    , m_fRotationSpeed( 45.0f )
    , m_vec3Movement( )
    , m_fRotation( 0.0f ) { }

FreeCamera::~FreeCamera( ) { }

bool FreeCamera::onKeyPress( KeyboardInputArgs& args )
{
    switch ( args.key )
    {
        case SDLK_a: m_vec3Movement.x = -m_fMovementSpeed;
            break;
        case SDLK_d: m_vec3Movement.x = m_fMovementSpeed;
            break;
        case SDLK_e: m_fRotation = -m_fRotationSpeed;
            break;
        case SDLK_q: m_fRotation = m_fRotationSpeed;
            break;
        case SDLK_s: m_vec3Movement.z = m_fMovementSpeed;
            break;
        case SDLK_w: m_vec3Movement.z = -m_fMovementSpeed;
            break;
    }

    //    print( m_vec3Movement, "m_vec3Movement" );
    //    print( m_fRotation, "m_fRotation" );

    return false;
}

bool FreeCamera::onKeyRelease( KeyboardInputArgs& args )
{
    switch ( args.key )
    {
        case SDLK_a:
        case SDLK_d:
        case SDLK_e:
        case SDLK_q:
        case SDLK_s:
        case SDLK_w:
            m_vec3Movement = Vector3( );
            m_fRotation = 0.0f;
            break;
    }

    return false;
}

void FreeCamera::update( )
{
    Camera::update( );

    if ( glm::length( m_vec3Movement ) > 0.0f || abs( m_fRotation ) > 0.0f )
    {
        getTransformation( )->rotate( m_fRotation * Time::deltaTime( ), getTransformation( )->up( ) );
        getTransformation( )->position( ) += getTransformation( )->transformDirection( m_vec3Movement * Time::deltaTime( ) );
        lookAt( getTransformation( )->position( ) + getTransformation( )->forward( ) );

//        print ( getTransformation( )->forward( ), "fwd" );


    }

}