/* 
 * File:   FileUtils.h
 * Author: Freddi
 *
 * Created on 9. August 2013, 21:26
 */

#ifndef FILEUTILS_H
#define	FILEUTILS_H

#include "Types.h"

class File
{
public:
    static string load(const string& c_strFile);
    static bool exists(const string& c_strFile);
    //static bool save(const string& c_strFile, const string& c_strContent);
};

#endif	/* FILEUTILS_H */

