/* 
 * File:   RenderTexture.h
 * Author: Freddi
 *
 * Created on 4. August 2013, 21:44
 */

#ifndef RENDERTEXTURE_H
#define	RENDERTEXTURE_H

#include "Texture.h"

class RenderTexture : public Texture
{
public:
    RenderTexture( );
    virtual ~RenderTexture( );

    void begin( );
    void end( );

private:
    GLuint m_uiColorAttachment;
    GLuint m_uiDepthAttachment;
    GLuint m_uiStencilAttachment;
};

#endif	/* RENDERTEXTURE_H */

