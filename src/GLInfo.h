#ifndef GLINFO_H
#define GLINFO_H

#include <set>
#include <string>

using namespace std;

class GLInfo
{
  public:

    static const GLInfo& get();

    /** Default destructor */
    virtual ~GLInfo();

    bool hasExtension(const string& c_strName);

    void printOpenGLInfo() const;

  protected:
  private:

    static GLInfo* smp_glInfo;

    /** Default constructor */
    GLInfo();
    GLInfo(const GLInfo& other) { };
};

#endif // AI_GLINFO_H
