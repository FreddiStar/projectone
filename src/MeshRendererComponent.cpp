/* 
 * File:   MeshRendererComponent.cpp
 * Author: Freddi
 * 
 * Created on 24. August 2013, 12:59
 */

#include "MeshRendererComponent.h"
#include "MeshComponent.h"
#include "GameObject.h"

MeshRendererComponent::MeshRendererComponent() {
}

MeshRendererComponent::~MeshRendererComponent() {
}

void
MeshRendererComponent::draw()
{
    GameObjectPtr p_obj = getGameObject();
    if (p_obj && mp_material)
    {
        mp_material->draw(p_obj);
    }
}