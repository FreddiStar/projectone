/* 
 * File:   SceneManager.h
 * Author: Freddi
 *
 * Created on 2. September 2013, 21:42
 */

#ifndef SCENEMANAGER_H
#define	SCENEMANAGER_H

#include "Scene.h"
#include "Physics.h"

class SceneManager {
public:
    
    static SceneManager& get();
    static void destroy();
    virtual ~SceneManager();
    
    ScenePtr getScene();
    void setScene(const ScenePtr& p_scene);
    
private:
    
    static SceneManager* smp_singleton;
    
    ScenePtr    mp_scene;
    
    SceneManager();
    SceneManager(const SceneManager& orig);
};

#endif	/* SCENEMANAGER_H */

