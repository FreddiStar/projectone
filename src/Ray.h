/* 
 * File:   Ray.h
 * Author: Freddi
 *
 * Created on 21. August 2013, 20:26
 */

#ifndef RAY_H
#define	RAY_H

#include "boost/shared_ptr.hpp"
#include "Types.h"

class Ray;
typedef boost::shared_ptr<Ray> RayPtr;

class Ray {
public:
    Ray(const Vector3& vecPosition = Vector3(0.0f), const Vector3& vecDirection = Vector3(0.0f, 0.0f, 1.0f));
    virtual ~Ray();
private:
    Vector3 m_vecPosition;
    Vector3 m_vecDirection;
};

#endif	/* RAY_H */

