/* 
 * File:   Time.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 22:21
 */

#ifndef TIME_H
#define	TIME_H

class Time {
public:
    Time();
    virtual ~Time();
    
    static float deltaTime();

private:
    friend class Application;
    static float sm_fDeltaTime;

};

#endif	/* TIME_H */

