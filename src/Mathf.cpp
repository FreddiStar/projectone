/* 
 * File:   Mathf.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 22:13
 */

#include <cmath>

#include "Mathf.h"

float Mathf::cos(float f)
{
    return std::cos(f);
}

float Mathf::sin(float f)
{
    return std::sin(f);
}

float Mathf::tan(float f)
{
    return std::tan(f);
}

float Mathf::deg2rad(float d)
{
    return d * (M_PI/180.0f);
}

float Mathf::rad2deg(float r)
{
    return r * (180.0f/M_PI);
}