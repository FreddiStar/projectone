/* 
 * File:   InputManager.h
 * Author: Freddi
 *
 * Created on 20. November 2013, 21:30
 */

#ifndef INPUTMANAGER_H
#define	INPUTMANAGER_H

#include <list>
#include "boost/shared_ptr.hpp"

using namespace std;

class MouseInputListener;
class KeyboardInputListener;
struct MouseInputArgs;
struct KeyboardInputArgs;

class InputManager;
typedef boost::shared_ptr<InputManager> InputManagerPtr;

class InputManager {
public:
    
    static InputManagerPtr get();
    static void destory();
    
    virtual ~InputManager();
    
    void addListener(MouseInputListener* p_listener);
    void addListener(KeyboardInputListener* p_listener);
    
    void removeListener(MouseInputListener* p_listener);
    void removeListener(KeyboardInputListener* p_listener);
    
    void onMouseMove(MouseInputArgs& args);
    void onMouseEnter(MouseInputArgs& args);
    void onMouseLeave(MouseInputArgs& args);
    void onMouseClick(MouseInputArgs& args);
    void onMouseRelease(MouseInputArgs& args);
    
    
    /* @return true if Keyevent was processed */
    bool onKeyPress(KeyboardInputArgs& args);
    
    /* @return true if Keyevent was processed */
    bool onKeyRelease(KeyboardInputArgs& args);
private:
    InputManager();
    InputManager(const InputManager& orig);
    
    static InputManagerPtr       p_instance;
    
    list<KeyboardInputListener*> m_listKeyboardInput;
    list<MouseInputListener*>    m_listMouseInput;
};

#endif	/* INPUTMANAGER_H */

