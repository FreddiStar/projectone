/* 
 * File:   UIElement.h
 * Author: Freddi
 *
 * Created on 30. September 2013, 22:02
 */

#ifndef UIELEMENT_H
#define	UIELEMENT_H

#include "GameObject.h"

class UIElement : public GameObject {
public:
    UIElement();
    virtual ~UIElement();
    
    virtual void init();
private:

};

#endif	/* UIELEMENT_H */

