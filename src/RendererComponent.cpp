/* 
 * File:   Renderer.cpp
 * Author: Freddi
 * 
 * Created on 8. August 2013, 20:56
 */

#include "RendererComponent.h"


RendererComponent::RendererComponent() {
}

RendererComponent::~RendererComponent() {
}

void
RendererComponent::setMaterial(const MaterialPtr& p_mat)
{
    mp_material = p_mat;
}

const MaterialPtr& 
RendererComponent::getMaterial()
{
    return mp_material;
}