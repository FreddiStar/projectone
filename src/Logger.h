/* 
 * File:   Logger.h
 * Author: Freddi
 *
 * Created on 29. Januar 2014, 21:33
 */

#ifndef LOGGER_H
#define	LOGGER_H

class Logger
{
public:
    static Logger& get( );
    static void destory( );
    virtual ~Logger( );
private:
    Logger( );
    static Logger* smp_singleton;

};

inline Logger& Log( )
{
    return Logger::get();
}

#endif	/* LOGGER_H */

