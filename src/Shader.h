/* 
 * File:   Shader.h
 * Author: frederiktrojahn
 *
 * Created on 3. August 2013, 13:32
 */

#ifndef SHADER_H
#define	SHADER_H

#include "boost/shared_ptr.hpp"
#include "Types.h"
#include "OpenGL.h"
#include "Texture2D.h"

class Shader;
typedef boost::shared_ptr<Shader> ShaderPtr;

struct ShaderVariables
{
    // Uniforms
    static const string UniformMVPMatrix;
    static const string UniformModelMatrix;
    static const string UniformViewMatrix;
    static const string UniformProjectionMatrix;

    // Textures
    static const string UniformColorTexture2D;
    static const string UniformNormalMap2D;
    static const string UniformSpecularMap2D;
    static const string UniformBumpMap2D;
    static const string UniformDisplacementMap2D;
    static const string UniformEnvironmentMap2D;

    // Misc
    static const string UniformDiffuseColorVec4;

    // Attribute names
    static const string AttributeVertexVec3;
    static const string AttributeColorVec4;
    static const string AttributeUVVec2;
    static const string AttributeNormalVec3;

    // Attribute location
    static const GLint AttributeLocationVertex;
    static const GLint AttributeLocationColor;
    static const GLint AttributeLocationUV;
    static const GLint AttributeLocationNormal;
};

class Shader
{
public:
    Shader( );
    virtual ~Shader( );

    GLint getUniform( const string& c_strUniform );
    GLint getAttribute( const string& c_strAttrib );

    void setUniform( GLint iLocation, const Matrix4x4& mat );
    void setUniform( GLint iLocation, const Vector4& vec4 );
    void setUniform( GLint iLocation, const Vector3& vec3 );
    void setUniform( GLint iLocation, const Vector2& vec2 );
    void setUniform( GLint iLocation, const Texture2DPtr& tex2d, GLint iTextureUnit );

    void begin( );
    void end( );

    // Create-Functions
    static ShaderPtr fromFilePair( const string& c_strVertex, const string& c_strFrag = "" );
    static ShaderPtr fromMemory( const char* cp_dataVertex, const char* cp_dataFrag );
private:

    static string FILE_EXT_FRAG;
    static string FILE_EXT_VERT;

    GLuint m_iProgramID;
};

#endif	/* SHADER_H */

