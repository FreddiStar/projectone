/* 
 * File:   DiffuseColorMaterial.h
 * Author: Freddi
 *
 * Created on 2. September 2013, 19:47
 */

#ifndef DIFFUSECOLORMATERIAL_H
#define	DIFFUSECOLORMATERIAL_H

#include "Material.h"

class DiffuseColorMaterial;
typedef boost::shared_ptr<DiffuseColorMaterial> DiffuseColorMaterialPtr;

class DiffuseColorMaterial : public Material {
public:
    DiffuseColorMaterial(const Vector4& color = Vector4(1.0f, 1.0f, 1.0f, 1.0f));
    virtual ~DiffuseColorMaterial();
    
    bool loadShader();
    
    void setUniforms(const GameObjectPtr& p_obj);
    
    void setColor(const Vector4& color);
    const Vector4& getColor() const;
    
    void getUniformsFromShader();
    
private:
    GLUniform   m_iUniformColor;
    Vector4     m_vec4Color;
};

#endif	/* DIFFUSECOLORMATERIAL_H */

