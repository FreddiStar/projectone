/* 
 * File:   main.cpp
 * Author: frederiktrojahn
 *
 * Created on 29. Juli 2013, 22:48
 */

#include <cstdlib>

#include "Application.h"
#include "Image.h"
#include "config.h"

using namespace std;

string get_bits( uint32_t x )
{
    string ret;
    for ( unsigned int mask = 0x80000000; mask; mask >>= 1 )
    {
        ret += (x & mask) ? "1" : "0";
    }
    return ret;
}

/*
 * 
 */
int main( int argc, char** argv )
{
//    uint16_t x = 0xa0f1;
//    uint16_t y = 0x0501;
//
//    uint32_t r = static_cast<uint32_t>(x);
//    r = r << 16;
//    r = r | static_cast<uint32_t>(y);
//
//    cout << get_bits(x) << endl;
//    cout << get_bits(y) << endl;
//    cout << get_bits(r) << endl;
//    cout << r << endl;

    Application app;
    if ( app.init( argc, argv ) )
    {
        app.run( );
        app.quit( );
    }
    return 0;
}

