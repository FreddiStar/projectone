/* 
 * File:   Component.cpp
 * Author: frederiktrojahn
 * 
 * Created on 2. August 2013, 17:44
 */

#include "Component.h"
#include "GameObject.h"

Component::Component() 
  : mwp_gameObject( )
  , m_bEnabled( true )
{
}

Component::~Component() 
{
}

GameObjectPtr 
Component::getGameObject()
{
    return mwp_gameObject.lock();
}

void 
Component::setGameObject(const GameObjectPtr& p_obj)
{
    if (p_obj)
        mwp_gameObject = p_obj;
}

void 
Component::enable()
{
    m_bEnabled = true;
}

void 
Component::disable()
{
    m_bEnabled = false;
}

bool 
Component::isEnabled()
{
    return m_bEnabled;
}