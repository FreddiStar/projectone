/* 
 * File:   MeshRendererComponent.h
 * Author: Freddi
 *
 * Created on 24. August 2013, 12:59
 */

#ifndef MESHRENDERERCOMPONENT_H
#define	MESHRENDERERCOMPONENT_H

#include "boost/shared_ptr.hpp"
#include "Material.h"
#include "RendererComponent.h"

class MeshRendererComponent;
typedef boost::shared_ptr<MeshRendererComponent> MeshRendererComponentPtr;

class MeshRendererComponent : public RendererComponent {
public:
    MeshRendererComponent();
    virtual ~MeshRendererComponent();
    
    void draw();
private:
};

#endif	/* MESHRENDERERCOMPONENT_H */

