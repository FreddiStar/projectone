/* 
 * File:   MeshComponent.h
 * Author: Freddi
 *
 * Created on 8. August 2013, 20:51
 */

#ifndef MESHCOMPONENT_H
#define	MESHCOMPONENT_H

#include "Component.h"
#include "Mesh.h"

class MeshComponent;
typedef boost::shared_ptr<MeshComponent> MeshComponentPtr;

class MeshComponent : public Component {
public:
    MeshComponent();
    virtual ~MeshComponent();
    
    MeshPtr getMesh();
    void setMesh(const MeshPtr& p_mesh);
private:
    MeshPtr mp_mesh;
};

#endif	/* MESHCOMPONENT_H */

