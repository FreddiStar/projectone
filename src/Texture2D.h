/* 
 * File:   Texture2D.h
 * Author: Freddi
 *
 * Created on 4. August 2013, 21:43
 */

#ifndef TEXTURE2D_H
#define	TEXTURE2D_H

#include "Texture.h"
#include "Image.h"
#include "OpenGL.h"

class Texture2D;
typedef boost::shared_ptr<Texture2D> Texture2DPtr;

class Texture2D : public Texture
{
public:
    Texture2D( const ImagePtr& p_img,
               GLenum textureTarget = GL_TEXTURE_2D,
               GLint mipmapLevel = 0,
               GLint internalFormat = GL_RGB,
               GLenum pixelFormat = GL_RGB,
               GLenum pixelType = GL_UNSIGNED_BYTE );
    virtual ~Texture2D( );

    void createTexture( );

private:
    ImagePtr mp_imgData;
    GLenum m_eTextureTarget;
    GLint m_iMipmapLevel;
    GLint m_iInternalFormat;
    GLenum m_ePixelFormat;
    GLenum m_ePixelType;
};

#endif	/* TEXTURE2D_H */

