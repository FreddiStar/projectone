/* 
 * File:   Material.cpp
 * Author: frederiktrojahn
 * 
 * Created on 3. August 2013, 13:34
 */

#include "Material.h"
#include "GameObject.h"
#include "Camera.h"
#include "MeshComponent.h"

Material::Material()
    : mp_shader()
    , m_iShaderUniformMVP( -1 )
    , m_bMeshGotAttributesFromShader( false )
{
}

Material::~Material() {
}

bool
Material::setup()
{
    if (!loadShader())
    {
        cout << "could not load shader" << endl;
        return false;
    }
    
    getUniformsFromShader();
    
    return true;
}

void
Material::getUniformsFromShader()
{
    m_iShaderUniformMVP = mp_shader->getUniform(ShaderVariables::UniformMVPMatrix);
}

void
Material::setUniforms(const GameObjectPtr& p_obj)
{
    //MVP Matrix
    // @todo Maybe pass it separately. Let Shader decide?
    Matrix4x4 MVP = Camera::getCurrent()->getProjectionMatrix() 
            * Camera::getCurrent()->getViewMatrix() 
            * p_obj->getTransformation()->getModelMatrix();
    mp_shader->setUniform(m_iShaderUniformMVP, MVP);
//    print(Camera::getCurrent()->getProjectionMatrix(), "Camera::getCurrent()->getProjectionMatrix");
//    print(Camera::getCurrent()->getViewMatrix(), "Camera::getCurrent()->getViewMatrix()");
//    print(p_obj->getTransformation()->getModelMatrix(), "p_obj->getTransformation()->getModelMatrix()");
//    print(MVP);
}

void
Material::draw(const GameObjectPtr& p_obj)
{
    if (!mp_shader) return;
    
    MeshComponentPtr p_meshComp = p_obj->getComponent<MeshComponent>();
    if (p_meshComp)
    {
        mp_shader->begin();
        
        setUniforms(p_obj);
        
        MeshPtr p_mesh = p_meshComp->getMesh();
        
        p_mesh->draw();
        
        mp_shader->end();
    }
    
    glCheckError("Material::draw");
}