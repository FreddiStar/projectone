/* 
 * File:   TextureAtlasRect.h
 * Author: Freddi
 *
 * Created on 28. Januar 2014, 15:12
 */

#ifndef TEXTUREATLASRECT_H
#define	TEXTUREATLASRECT_H

#include "Types.h"

class TextureAtlasRect;
typedef boost::shared_ptr<TextureAtlasRect> TextureAtlasRectPtr;

class TextureAtlasRect
{
public:
    TextureAtlasRect( uint32_t uiAtlasWidth,
                      uint32_t uiAtlasHeight,
                      uint32_t uiLeft,
                      uint32_t uiRight,
                      uint32_t uiTop,
                      uint32_t uiBottom );
    virtual ~TextureAtlasRect( );
    
    Vector2 getTopLeft() const;
    Vector2 getTopRight() const;
    Vector2 getBottomLeft() const;
    Vector2 getBottomRight() const;
    
    
private:
    float m_fLeft;
    float m_fRight;
    float m_fTop;
    float m_fBottom;

    uint32_t m_uiWidth;
    uint32_t m_uiHeight;
    
    uint32_t m_uiAtlasWidth;
    uint32_t m_uiAtlasHeight;
    
    uint32_t m_uiLeft;
    uint32_t m_uiRight;
    uint32_t m_uiTop;
    uint32_t m_uiBottom;


};

#endif	/* TEXTUREATLASRECT_H */

