/* 
 * File:   LuaState.cpp
 * Author: Freddi
 * 
 * Created on 28. November 2013, 20:37
 */

#include "LuaState.h"

LuaStatePtr LuaState::mp_singleton = LuaStatePtr();

LuaState::LuaState()
    : mp_state( NULL )
{
    mp_state = luaL_newstate();
}

LuaState::~LuaState()
{
    lua_close(mp_state);
}

LuaStatePtr LuaState::get()
{
    if (!mp_singleton)
        mp_singleton.reset(new LuaState());
    
    return mp_singleton;
}
void LuaState::destory()
{
    mp_singleton.reset();
}