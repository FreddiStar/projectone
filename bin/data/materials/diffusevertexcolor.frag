#version 130

in vec4 vert_vec4Color;

out vec4 out_color;

void main()
{
	out_color = vert_vec4Color;
}
