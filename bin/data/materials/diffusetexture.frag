#version 130

uniform sampler2D tex2dColor;

in vec2 vert_vec2UV;

out vec4 out_color;

void main()
{
	out_color = texture2D(tex2dColor, vert_vec2UV.st);
}
