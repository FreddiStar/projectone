#version 130

uniform mat4 mat4MVP;

in vec3 vec3Vertex;
in vec2 vec2UV;

out vec2 vert_vec2UV;

void main()
{
	gl_Position = (mat4MVP * vec4(vec3Vertex, 1.0));
	vert_vec2UV = vec2UV;
}
