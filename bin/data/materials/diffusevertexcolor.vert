#version 130

uniform mat4 mat4MVP;

in vec3 vec3Vertex;
in vec4 vec4Color;

out vec4 vert_vec4Color;

void main()
{
	vert_vec4Color = vec4Color;
	gl_Position = (mat4MVP * vec4(vec3Vertex, 1.0f));
	
}
