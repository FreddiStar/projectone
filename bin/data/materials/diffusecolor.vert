#version 130

uniform vec4 vec4DiffuseColor;
uniform mat4 mat4MVP;

in vec3 vec3Vertex;
out vec4 vert_vec4Color;

void main()
{
	gl_Position = (mat4MVP * vec4(vec3Vertex, 1.0f));
	vert_vec4Color = vec4DiffuseColor;
}
