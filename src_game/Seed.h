/* 
 * File:   Seed.h
 * Author: frederiktrojahn
 *
 * Created on 3. Oktober 2013, 23:25
 */

#ifndef SEED_H
#define	SEED_H

class Seed {
public:
    Seed();
    virtual ~Seed();
    
    void set(const char* p_char);
private:
    int m_iSeed;
};

#endif	/* SEED_H */

