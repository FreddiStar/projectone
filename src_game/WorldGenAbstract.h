/* 
 * File:   WorldGenAbstract.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 15:11
 */

#ifndef WORLDGENABSTRACT_H
#define	WORLDGENABSTRACT_H

#include "WorldChunk.h"


class WorldGenAbstract
{
public:
    WorldGenAbstract( );
    virtual ~WorldGenAbstract( );
    
    virtual WorldChunkPtr generate(int16_t iChunkX, int16_t iChunkY) = 0;
private:

};

#endif	/* WORLDGENABSTRACT_H */

