/* 
 * File:   TestScene.cpp
 * Author: Freddi
 * 
 * Created on 2. September 2013, 21:50
 */

#include "TestScene.h"
#include "MeshComponent.h"
#include "MeshRendererComponent.h"
#include "Primitives.h"
#include "DiffuseColorMaterial.h"
#include "Time.h"
#include "Mathf.h"
#include "DiffuseVertexColorMaterial.h"
#include "DiffuseTextureMaterial.h"
#include "FreeCamera.h"
#include "Physics.h"
#include "TextureAtlas.h"
#include "WorldGenFlat.h"
#include "World.h"

TestScene::TestScene( )
    : Scene( )
    , fSum( 0.0f ) { }

TestScene::~TestScene( ) { }

GameObjectPtr TestScene::setup( MeshPtr p_mesh, MaterialPtr p_mat )
{
    GameObjectPtr obj = Instantiate<GameObject>();
    MeshComponentPtr p_meshComp( new MeshComponent( ) );
    MeshRendererComponentPtr p_meshRenderer( new MeshRendererComponent( ) );

    p_meshComp->setMesh( p_mesh );

    if ( !p_mat->setup( ) )
    {
        cout << "could not setup Material" << endl;
    }

    p_meshRenderer->setMaterial( p_mat );

    obj->addComponent( p_meshComp );
    obj->addComponent( p_meshRenderer );

    return obj;
}

void TestScene::setup( )
{
    glEnable( GL_DEPTH_TEST );

    TextureAtlas atlas;
    if ( atlas.load( "data/assets/test.atlas" ) )
    {
        //cout << "Atlas Loaded" << endl;
    }

    WorldGenFlatPtr p_flat( new WorldGenFlat( ) );
    World::get( ).setChunkByCoords( 0, 0, p_flat->generate( 0, 0 ) );

    mp_camera = Instantiate<Camera>();
    mp_camera->setActive( );

    mp_camera->setOrthographicOriginInCenter( true );
    mp_camera->setProjectionMods( Camera::eOrthographic );
    mp_camera->getTransformation( )->position( ) = Vector3( 0.0f, 0.0f, 10.0f );
    mp_camera->lookAt( Vector3( 0.0f, 0.0f, 0.0f ) );
    addGameObject( mp_camera );


    //    ImagePtr p_img = Image::fromFile( "data/assets/rainbow.png" );
    ImagePtr p_img = Image::fromFile( "data/assets/32x32.png" );

    GLenum eMode = GL_RGB;
    if ( p_img )
    {
        if ( p_img->getBytePerPixel( ) == 4 )
            eMode = GL_RGBA;
    }

    Texture2DPtr p_texture( new Texture2D( p_img, GL_TEXTURE_2D, 0, eMode, eMode ) );
    p_texture->createTexture( );
    MaterialPtr p_materialPlane( new DiffuseTextureMaterial( p_texture ) );
    mp_plane = setup( Primitives::QuadsPlane( 8, 8 ), p_materialPlane );

    MaterialPtr p_matAxis( new DiffuseVertexColorMaterial( ) );

    mp_plane->getTransformation( )->position( ) = Vector3( 0.0f, 0.0f, 0.0f );

    addGameObject( mp_plane );

    glCheckError( "TestScene::setup" );
}

void
TestScene::update( )
{
    Scene::update( );

    fSum += Time::deltaTime( );

    //    if (mp_plane)
    //    {
    //        mp_plane->getTransformation()->position() = Vector3(Mathf::sin(fSum)*3.0f, Mathf::cos(fSum)*3.0f, 0.0f);
    //    }

    //    if (mp_axis)
    //    {
    //        mp_axis->getTransformation()->lookAt(mp_plane->getTransformation()->position());
    //    }

    //    if (mp_camera)
    //    {
    //        mp_camera->lookAt(mp_plane->getTransformation()->position());
    //    }
}