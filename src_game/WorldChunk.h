/* 
 * File:   WorldChunk2D.h
 * Author: Freddi
 *
 * Created on 27. Januar 2014, 12:45
 */

#ifndef WORLDCHUNK_H
#define	WORLDCHUNK_H

#include "stdint.h"
#include <map>

using namespace std;

#include "boost/shared_ptr.hpp"

class WorldTile;
typedef boost::shared_ptr<WorldTile> WorldTilePtr;

class WorldChunk;
typedef boost::shared_ptr<WorldChunk> WorldChunkPtr;

struct WorldChunkCoords
{

    WorldChunkCoords( int16_t _x, int16_t _y ) : x( _x ), y( _y )
    {
    }
    int16_t x;
    int16_t y;

    uint32_t hash( ) const
    {
        uint32_t r = static_cast<uint32_t> (x);
        r = r << 16;
        r = r | static_cast<uint32_t> (y);

        return r;
    }
};

struct WorldChunkCoordsLess : public less<WorldChunkCoords>
{

    bool operator() (const WorldChunkCoords& x, const WorldChunkCoords& y) const
    {
        return x.hash() < y.hash();
    }
};

typedef map<WorldChunkCoords, WorldChunkPtr, WorldChunkCoordsLess> WorldChunkMap;

class WorldChunk
{
public:
    
    typedef map<uint32_t, WorldTilePtr> MapIndexToTile;
    
    WorldChunk( int16_t iChunkX, int16_t iChunkY );
    virtual ~WorldChunk( );

    static const uint8_t gs_uiChunkDimensions;
    static const uint16_t gs_uiChunkHeight;

    static void convertWorldToChunkCoordsXY( int iWorldX, int iWorldY, int16_t& iLocalX, int16_t& iLocalY );
    void convertWorldToLocalCoordsXY( int iWorldX, int iWorldY, uint8_t& iLocalX, uint8_t& iLocalY );

    WorldTilePtr getTile( uint8_t iLocalX, uint8_t iLocalY, int16_t layer );
    void setTile( uint8_t iLocalX, uint8_t iLocalY, int16_t layer, const WorldTilePtr& p_tile );

private:

    friend class World;

    void setCoordinates( int16_t x, int16_t y );

    int16_t m_uiChunkX;
    int16_t m_uiChunkY;
    
    uint32_t indexFromCoords(uint8_t x, uint8_t y, int16_t z);
    MapIndexToTile m_mapIndexToTile;
};

#endif	/* WORLDCHUNK2D_H */

