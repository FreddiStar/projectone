/* 
 * File:   WorldTile2D.cpp
 * Author: Freddi
 * 
 * Created on 27. Januar 2014, 12:50
 */

#include "WorldTile.h"
#include "WorldTileGrass.h"
#include "WorldTileStone.h"

const WorldTilePtr WorldTile::grass( new WorldTileGrass( "tile:grass", TileMaterialPtr( new TileMaterial( ) ) ) );
const WorldTilePtr WorldTile::stone( new WorldTileStone( "tile:stone", TileMaterialPtr( new TileMaterial( ) ) ) );

WorldTile::WorldTile( const string& strID, const TileMaterialPtr& p_material )
    : m_strTileID( strID )
    , mp_material( p_material ) { }

WorldTile::~WorldTile( ) { }

