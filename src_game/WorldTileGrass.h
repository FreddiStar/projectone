/* 
 * File:   WorldTile2DGrass.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 13:06
 */

#ifndef WORLDTILEGRASS_H
#define	WORLDTILEGRASS_H

#include "TileMaterial.h"
#include "WorldTile.h"


class WorldTileGrass;
typedef boost::shared_ptr<WorldTileGrass> WorldTileGrassPtr;

class WorldTileGrass : public WorldTile
{
public:
    WorldTileGrass( const string& strID, const TileMaterialPtr& p_material );
    virtual ~WorldTileGrass( );
private:

};

#endif	/* WORLDTILE2DGRASS_H */

