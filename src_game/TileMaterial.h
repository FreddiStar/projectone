/* 
 * File:   Tile2DMaterial.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 13:33
 */

#ifndef TILEMATERIAL_H
#define	TILEMATERIAL_H

#include "boost/shared_ptr.hpp"

class TileMaterial;
typedef boost::shared_ptr<TileMaterial> TileMaterialPtr;

class TileMaterial
{
public:
    TileMaterial( );
    virtual ~TileMaterial( );
private:

};

#endif	/* TILE2DMATERIAL_H */

