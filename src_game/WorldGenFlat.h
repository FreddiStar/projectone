/* 
 * File:   WorldGenFlat.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 15:13
 */

#ifndef WORLDGENFLAT_H
#define	WORLDGENFLAT_H

#include "WorldChunk.h"
#include "WorldGenAbstract.h"


class WorldGenFlat;
typedef boost::shared_ptr<WorldGenFlat> WorldGenFlatPtr;

class WorldGenFlat : public WorldGenAbstract
{
public:
    WorldGenFlat( );
    virtual ~WorldGenFlat( );
    
    virtual WorldChunkPtr generate( int16_t iChunkX, int16_t iChunkY );

private:

};

#endif	/* WORLDGENFLAT_H */

