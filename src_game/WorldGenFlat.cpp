/* 
 * File:   WorldGenFlat.cpp
 * Author: Freddi
 * 
 * Created on 4. Februar 2014, 15:13
 */

#include "WorldGenFlat.h"
#include "WorldTile.h"

WorldGenFlat::WorldGenFlat( ) { }

WorldGenFlat::~WorldGenFlat( ) { }

WorldChunkPtr WorldGenFlat::generate( int16_t iChunkX, int16_t iChunkY )
{
    WorldChunkPtr p_chunk( new WorldChunk( iChunkX, iChunkY ) );

    for ( uint8_t y = 0; y < WorldChunk::gs_uiChunkDimensions; ++y )
    {
        for ( uint8_t x = 0; x < WorldChunk::gs_uiChunkDimensions; ++x )
        {
            for ( uint8_t l = 0; l <= 6; ++l )
            {
                if (l < 6)
                {
                    p_chunk->setTile(x, y, l, WorldTile::stone);
                }
                else
                {
                    p_chunk->setTile(x, y, l, WorldTile::grass);
                }
            }
        }
    }

    return p_chunk;
}