/* 
 * File:   WorldChunk2D.cpp
 * Author: Freddi
 * 
 * Created on 27. Januar 2014, 12:45
 */

#include <vector>

#include "WorldChunk.h"
#include "WorldTile.h"

const uint8_t WorldChunk::gs_uiChunkDimensions = 8;
const uint16_t WorldChunk::gs_uiChunkHeight = 256;

WorldChunk::WorldChunk( int16_t iChunkX, int16_t iChunkY )
    : m_uiChunkX( iChunkX )
    , m_uiChunkY( iChunkY )
    , m_mapIndexToTile( ) { }

WorldChunk::~WorldChunk( ) { }

WorldTilePtr WorldChunk::getTile( uint8_t iLocalX, uint8_t iLocalY, int16_t layer )
{
    MapIndexToTile::iterator it = m_mapIndexToTile.find( indexFromCoords( iLocalX, iLocalY, layer ) );
    if ( it != m_mapIndexToTile.end( ) )
        return it->second;
    return WorldTilePtr( );
}

uint32_t WorldChunk::indexFromCoords( uint8_t x, uint8_t y, int16_t z )
{
    uint32_t r = static_cast<uint32_t>(z);
    r = r << 16;
    r = r | static_cast<uint32_t>(y);
    r = r << 8;
    r = r | static_cast<uint32_t>(x);

    return r;
}

void WorldChunk::setTile( uint8_t iLocalX, uint8_t iLocalY, int16_t layer, const WorldTilePtr& p_tile )
{
    uint32_t index = indexFromCoords( iLocalX, iLocalY, layer );
    
    MapIndexToTile::iterator it = m_mapIndexToTile.find( index );

    if ( it != m_mapIndexToTile.end( ) )
        m_mapIndexToTile.erase( it );

    if ( p_tile )
        m_mapIndexToTile.insert( std::make_pair( index, p_tile ) );

}

void WorldChunk::setCoordinates( int16_t x, int16_t y )
{
    m_uiChunkX = x;
    m_uiChunkY = y;
}

void WorldChunk::convertWorldToChunkCoordsXY( int iWorldX, int iWorldY, int16_t& iLocalX, int16_t& iLocalY )
{
    iLocalX = static_cast<int16_t>(iWorldX / gs_uiChunkDimensions);
    iLocalY = static_cast<int16_t>(iWorldY / gs_uiChunkDimensions);
}

void WorldChunk::convertWorldToLocalCoordsXY( int iWorldX, int iWorldY, uint8_t& iLocalX, uint8_t& iLocalY )
{
    iLocalX = static_cast<uint8_t>(iWorldX - m_uiChunkX * gs_uiChunkDimensions);
    iLocalY = static_cast<uint8_t>(iWorldX - m_uiChunkY * gs_uiChunkDimensions);
}