/* 
 * File:   WorldTileStone.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 15:45
 */

#ifndef WORLDTILESTONE_H
#define	WORLDTILESTONE_H

#include "WorldTile.h"

class WorldTileStone;
typedef boost::shared_ptr<WorldTileStone> WorldTileStonePtr;

class WorldTileStone : public WorldTile
{
public:
    WorldTileStone( const string& strID, const TileMaterialPtr& p_material );
    virtual ~WorldTileStone( );
private:

};

#endif	/* WORLDTILESTONE_H */

