/* 
 * File:   World2D.h
 * Author: Freddi
 *
 * Created on 27. Januar 2014, 12:51
 */

#ifndef WORLD2D_H
#define	WORLD2D_H

#include "WorldChunk.h"
#include "WorldTile.h"

class World
{
public:

    static World& get( );
    static void destory( );

    virtual ~World( );

    void registerTile( const WorldTilePtr& p_tile );
    WorldTilePtr getTile(const string& c_strTileID);

    WorldChunkPtr getChunk( int iWorldX, int iWorldY );
    WorldChunkPtr getChunkByCoords( int16_t iChunkX, int16_t iChunkY );
    
    void setChunk( int iWorldX, int iWorldY, const WorldChunkPtr& p_chunk );
    void setChunkByCoords( int16_t iChunkX, int16_t iChunkY, const WorldChunkPtr& p_chunk );
    
    WorldTilePtr getTile( int iWorldX, int iWorldY, uint16_t layer );
    void setTile( int iWorldX, int iWorldY, int16_t layer, const WorldTilePtr& p_tile );


private:
    World( );

    static World* mp_singleton;
    
    WorldChunkMap m_mapChunks;
    
    WorldTile::MapIDToWorldTilePtr m_mapIDToTile;
};

#endif	/* WORLD2D_H */

