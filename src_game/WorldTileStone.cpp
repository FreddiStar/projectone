/* 
 * File:   WorldTileStone.cpp
 * Author: Freddi
 * 
 * Created on 4. Februar 2014, 15:45
 */

#include "WorldTileStone.h"

WorldTileStone::WorldTileStone( const string& strID, const TileMaterialPtr& p_material )
    : WorldTile( strID, p_material ) { }

WorldTileStone::~WorldTileStone( ) { }

