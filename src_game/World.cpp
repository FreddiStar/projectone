/* 
 * File:   World2D.cpp
 * Author: Freddi
 * 
 * Created on 27. Januar 2014, 12:51
 */

#include "World.h"
#include "Utils.h"
#include "WorldChunk.h"

World* World::mp_singleton = NULL;

World& World::get( )
{
    if ( !mp_singleton )
        mp_singleton = new World( );

    return *mp_singleton;
}

void World::destory( )
{
    safeDelete( mp_singleton );
}

World::World( )
    : m_mapChunks( ) { }

World::~World( ) { }

void World::registerTile( const WorldTilePtr& p_tile )
{
    if ( p_tile )
        m_mapIDToTile.insert( std::make_pair( p_tile->getTileID( ), p_tile ) );
}

WorldTilePtr World::getTile( const string& c_strTileID )
{
    WorldTile::MapIDToWorldTilePtr::iterator it = m_mapIDToTile.find( c_strTileID );
    if ( it != m_mapIDToTile.end( ) )
        return it->second;

    return WorldTilePtr( );
}

WorldChunkPtr World::getChunk( int iWorldX, int iWorldY )
{
    int16_t iChunkX = 0, iChunkY = 0;
    WorldChunk::convertWorldToChunkCoordsXY( iWorldX, iWorldY, iChunkX, iChunkY );

    return getChunkByCoords( iChunkX, iChunkY );
}

WorldChunkPtr World::getChunkByCoords( int16_t iChunkX, int16_t iChunkY )
{
    WorldChunkMap::iterator it = m_mapChunks.find( WorldChunkCoords( iChunkX, iChunkY ) );
    if ( it != m_mapChunks.end( ) )
    {
        return it->second;
    }
}

WorldTilePtr World::getTile( int iWorldX, int iWorldY, uint16_t layer )
{
    WorldChunkPtr p_chunk = getChunk( iWorldX, iWorldY );
    if ( p_chunk )
    {
        uint8_t iChunkX = 0;
        uint8_t iChunkY = 0;
        p_chunk->convertWorldToLocalCoordsXY( iWorldX, iWorldY, iChunkX, iChunkY );

        return p_chunk->getTile( iChunkX, iChunkY, layer );
    }

    return WorldTilePtr( );
}

void World::setTile( int iWorldX, int iWorldY, int16_t layer, const WorldTilePtr& p_tile )
{
    WorldChunkPtr p_chunk = getChunk( iWorldX, iWorldY );
    if ( p_chunk )
    {
        uint8_t iLocalX = 0;
        uint8_t iLocalY = 0;
        p_chunk->convertWorldToLocalCoordsXY( iWorldX, iWorldY, iLocalX, iLocalY );

        return p_chunk->setTile( iLocalX, iLocalY, layer, p_tile );
    }
}

void World::setChunk( int iWorldX, int iWorldY, const WorldChunkPtr& p_chunk )
{
    int16_t iChunkX = static_cast<int16_t>(iWorldX / WorldChunk::gs_uiChunkDimensions);
    int16_t iChunkY = static_cast<int16_t>(iWorldY / WorldChunk::gs_uiChunkDimensions);

    setChunkByCoords( iChunkX, iChunkY, p_chunk );
}

void World::setChunkByCoords( int16_t iChunkX, int16_t iChunkY, const WorldChunkPtr& p_chunk )
{
    if ( p_chunk )
    {
        p_chunk->setCoordinates( iChunkX, iChunkY );
        WorldChunkCoords coords( iChunkX, iChunkY );

        m_mapChunks.erase( coords );
        m_mapChunks.insert( std::make_pair( coords, p_chunk ) );
    }
}