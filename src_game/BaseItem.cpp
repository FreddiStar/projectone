/* 
 * File:   BaseItem.cpp
 * Author: Freddi
 * 
 * Created on 4. Februar 2014, 13:09
 */

#include "BaseItem.h"

BaseItem::BaseItem( const string& strID, const ItemMaterialPtr& p_material )
    : m_strItemID( strID )
    , mp_material( p_material ) { }

BaseItem::~BaseItem( ) { }

