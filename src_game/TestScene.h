/* 
 * File:   TestScene.h
 * Author: Freddi
 *
 * Created on 2. September 2013, 21:50
 */

#ifndef TESTSCENE_H
#define	TESTSCENE_H

#include "Scene.h"

class TestScene : public Scene
{
public:
    TestScene( );
    virtual ~TestScene( );

    void setup( );

    void update( );
private:
    
    GameObjectPtr setup(MeshPtr p_mesh, MaterialPtr p_mat);
    
    GameObjectPtr mp_plane;
    
//    GameObjectPtr mp_axis;
//    GameObjectPtr mp_target;
    
    float fSum;
    CameraPtr mp_camera;
};

#endif	/* TESTSCENE_H */

