/* 
 * File:   WorldTileLayer2D.cpp
 * Author: Freddi
 * 
 * Created on 27. Januar 2014, 12:55
 */

#include "WorldChunkTileLayer.h"
#include "WorldChunk.h"

WorldChunkTileLayer::WorldChunkTileLayer( const WorldChunkPtr& p_chunk, uint16_t layer )
    : m_vecTiles( )
{
    if (p_chunk)
    {
        loadLayer(p_chunk, layer);
    }
}

WorldChunkTileLayer::~WorldChunkTileLayer( ) { }

WorldTilePtr WorldChunkTileLayer::getTile( int x, int y )
{
    int index = y * WorldChunk::gs_uiChunkDimensions + x;
    return m_vecTiles[index];
}

void WorldChunkTileLayer::setTile( const WorldTilePtr& p_tile, int x, int y )
{
    int index = y * WorldChunk::gs_uiChunkDimensions + x;
    m_vecTiles[index] = p_tile;
}

void WorldChunkTileLayer::loadLayer( const WorldChunkPtr& p_chunk, uint16_t layer )
{
    m_vecTiles.reserve( WorldChunk::gs_uiChunkDimensions * WorldChunk::gs_uiChunkDimensions );
}