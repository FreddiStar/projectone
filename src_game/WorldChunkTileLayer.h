/* 
 * File:   WorldTileLayer2D.h
 * Author: Freddi
 *
 * Created on 27. Januar 2014, 12:55
 */

#ifndef WORLDTILELAYER_H
#define	WORLDTILELAYER_H

#include "boost/shared_ptr.hpp"
#include <vector>


#include "WorldTile.h"
#include "Mesh.h"


using namespace std;

class WorldChunkTileLayer;
typedef boost::shared_ptr<WorldChunkTileLayer> WorldChunkTileLayerPtr;

class WorldChunkTileLayer
{
public:
    WorldChunkTileLayer( const WorldChunkPtr& p_chunk = WorldChunkPtr( ), uint16_t layer = 0 );
    virtual ~WorldChunkTileLayer( );

    void loadLayer( const WorldChunkPtr& p_chunk, uint16_t layer );

    WorldTilePtr getTile( int x, int y );
    void setTile( const WorldTilePtr& p_tile, int x, int y );

    const MeshPtr& getMesh( ) const
    {
        return mp_meshLayer;
    }

private:
    vector<WorldTilePtr> m_vecTiles;

    MeshPtr mp_meshLayer;
};

#endif	/* WORLDTILELAYER2D_H */

