/* 
 * File:   WorldTile2D.h
 * Author: Freddi
 *
 * Created on 27. Januar 2014, 12:50
 */

#ifndef WORLDTILE_H
#define	WORLDTILE_H

#include "boost/shared_ptr.hpp"
#include "WorldChunk.h"
#include "TileMaterial.h"

#include <string>
using namespace std;

class WorldTile;
typedef boost::shared_ptr<WorldTile> WorldTilePtr;

class WorldTile
{
public:
    
    static const WorldTilePtr grass;
    static const WorldTilePtr stone;
    
    typedef map<string, WorldTilePtr> MapIDToWorldTilePtr;
    WorldTile( const string& strID, const TileMaterialPtr& p_material );
    virtual ~WorldTile( );

    void onTileTick( const WorldTilePtr& p_tile );
    void onTileNeighborUpdate( const WorldTilePtr& p_tile, int x, int y );
    void onTileMine( const WorldTilePtr& p_tile );

    void onClick( const WorldTilePtr& p_tile );

    const string& getTileID( ) const
    {
        return m_strTileID;
    }
private:
    string m_strTileID;
    TileMaterialPtr mp_material;
};

#endif	/* WORLDTILE_H */

