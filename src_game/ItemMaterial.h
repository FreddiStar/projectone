/* 
 * File:   ItemMaterial.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 13:45
 */

#ifndef ITEMMATERIAL_H
#define	ITEMMATERIAL_H

#include "boost/shared_ptr.hpp"

class ItemMaterial;
typedef boost::shared_ptr<ItemMaterial> ItemMaterialPtr;

class ItemMaterial
{
public:
    ItemMaterial( );
    virtual ~ItemMaterial( );
private: 

};

#endif	/* ITEMMATERIAL_H */

