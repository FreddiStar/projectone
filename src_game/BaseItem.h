/* 
 * File:   BaseItem.h
 * Author: Freddi
 *
 * Created on 4. Februar 2014, 13:09
 */

#ifndef BASEITEM_H
#define	BASEITEM_H

#include <string>

using namespace std;

#include "boost/shared_ptr.hpp"
#include "ItemMaterial.h"

class BaseItem;
typedef boost::shared_ptr<BaseItem> BaseItemPtr;

class BaseItem
{
public:
    BaseItem( const string& strID, const ItemMaterialPtr& p_material );
    virtual ~BaseItem( );
private:
    string m_strItemID;
    ItemMaterialPtr mp_material;
};

#endif	/* BASEITEM_H */

