/* 
 * File:   WorldTile2DGrass.cpp
 * Author: Freddi
 * 
 * Created on 4. Februar 2014, 13:06
 */

#include "WorldTileGrass.h"

WorldTileGrass::WorldTileGrass( const string& strID, const TileMaterialPtr& p_material )
    : WorldTile( strID, p_material ) { }

WorldTileGrass::~WorldTileGrass( ) { }

